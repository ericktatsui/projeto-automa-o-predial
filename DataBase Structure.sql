-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: automacao
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `conjunto`
--

DROP TABLE IF EXISTS `conjunto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conjunto` (
  `IDCONJUNTO` int(11) NOT NULL AUTO_INCREMENT,
  `ANDAR` int(11) DEFAULT NULL,
  `NUMERO` int(11) DEFAULT NULL,
  `BLOCO` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`IDCONJUNTO`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conjunto`
--

LOCK TABLES `conjunto` WRITE;
/*!40000 ALTER TABLE `conjunto` DISABLE KEYS */;
INSERT INTO `conjunto` VALUES (1,1,1,'A'),(2,1,2,'A'),(3,1,3,'A'),(4,1,4,'A'),(5,1,5,'A'),(6,2,1,'A'),(7,2,2,'A'),(8,2,3,'A'),(9,2,4,'A'),(10,2,5,'A'),(11,3,1,'A'),(12,3,2,'A'),(13,3,3,'A'),(14,3,4,'A'),(15,3,5,'A'),(16,4,1,'A'),(17,4,2,'A'),(18,4,3,'A'),(19,4,4,'A'),(20,4,5,'A'),(21,1,1,'B'),(22,1,2,'B'),(23,1,3,'B'),(24,1,4,'B'),(25,1,5,'B'),(26,2,1,'B'),(27,2,2,'B'),(28,2,3,'B'),(29,2,4,'B'),(30,2,5,'B'),(31,3,1,'B'),(32,3,2,'B'),(33,3,3,'B'),(34,3,4,'B'),(35,3,5,'B'),(36,4,1,'B'),(37,4,2,'B'),(38,4,3,'B'),(39,4,4,'B'),(40,4,5,'B'),(41,1,1,'C'),(42,1,2,'C'),(43,1,3,'C'),(44,1,4,'C'),(45,1,5,'C'),(46,2,1,'C'),(47,2,2,'C'),(48,2,3,'C'),(49,2,4,'C'),(50,2,5,'C'),(51,3,1,'C'),(52,3,2,'C'),(53,3,3,'C'),(54,3,4,'C'),(55,3,5,'C'),(56,4,1,'C'),(57,4,2,'C'),(58,4,3,'C'),(59,4,4,'C'),(60,4,5,'C');
/*!40000 ALTER TABLE `conjunto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `IDEMPRESA` int(11) NOT NULL AUTO_INCREMENT,
  `CNPJ` varchar(20) DEFAULT NULL,
  `RAZAOSOCIAL` varchar(100) DEFAULT NULL,
  `HRFUNCINI` varchar(5) DEFAULT NULL,
  `HRFUNCFIM` varchar(5) DEFAULT NULL,
  `TEMPMAX` int(11) DEFAULT NULL,
  `HRARINI` varchar(5) DEFAULT NULL,
  `HRARFIM` varchar(5) DEFAULT NULL,
  `CONJUNTO` int(11) DEFAULT NULL,
  PRIMARY KEY (`IDEMPRESA`),
  KEY `CONJUNTO_FK_idx` (`CONJUNTO`),
  CONSTRAINT `CONJUNTO_FK` FOREIGN KEY (`CONJUNTO`) REFERENCES `conjunto` (`IDCONJUNTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` VALUES (2,'65.793.678/5634-87','Teste LTDA','2:0','2:40',15,'7:20','11:20',2),(3,'98.576.775/6776-77','aehooo22','4:40','8:0',32,'13:20','20:0',9),(6,'99.999.999/9999-99','right','0:5','0:10',2,'0:15','0:25',52),(7,'46.457.658/3678-35','ydfhgsfdhgfdh','0:5','0:20',3,'0:30','13:10',2);
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funcionario`
--

DROP TABLE IF EXISTS `funcionario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funcionario` (
  `idFUNCIONARIO` int(11) NOT NULL AUTO_INCREMENT,
  `CPF` varchar(45) NOT NULL,
  `RG` varchar(45) DEFAULT NULL,
  `Nome` varchar(45) NOT NULL,
  `DtNasc` varchar(45) NOT NULL,
  `Email` varchar(45) DEFAULT NULL,
  `Telefone` varchar(45) DEFAULT NULL,
  `Celular` varchar(45) DEFAULT NULL,
  `HrIni` varchar(45) NOT NULL,
  `HrFim` varchar(45) NOT NULL,
  PRIMARY KEY (`idFUNCIONARIO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funcionario`
--

LOCK TABLES `funcionario` WRITE;
/*!40000 ALTER TABLE `funcionario` DISABLE KEYS */;
/*!40000 ALTER TABLE `funcionario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idUSER` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(45) NOT NULL,
  `CPF` varchar(45) NOT NULL,
  `RG` varchar(45) DEFAULT NULL,
  `DATANASCIMENTO` varchar(45) NOT NULL,
  `EMAIL` varchar(45) DEFAULT NULL,
  `TELEFONE` varchar(45) DEFAULT NULL,
  `CELULAR` varchar(45) DEFAULT NULL,
  `NIVEL` varchar(45) NOT NULL,
  PRIMARY KEY (`idUSER`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (2,'erwitgreh','874.378.364-85','7649653978','97/75/6476','767yhffed@fyedgfds.com','(47)6328-9536','(78)65947-6539','Funcionario');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'automacao'
--

--
-- Dumping routines for database 'automacao'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-19 21:51:12
