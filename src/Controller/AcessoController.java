package Controller;

import View.ConsultarAcessoView;

public class AcessoController extends Controller {
	private ConsultarAcessoView consultarView;
	
	public void renderConsultar()
	{
		consultarView = new ConsultarAcessoView( this );
		consultarView.show();
	}
}
