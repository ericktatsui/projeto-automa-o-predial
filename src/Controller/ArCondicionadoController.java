package Controller;

import java.util.ArrayList;

import Model.ArCondicionadoModel;
import View.ArCondicionadoView;

public class ArCondicionadoController extends Controller {
	private ArCondicionadoView view;
	private ArCondicionadoModel model;
	
	public ArCondicionadoController()
	{
		view = new ArCondicionadoView( this );
		model = new ArCondicionadoModel();
	}
	
	public void render()
	{
		view.show();
	}
	
	public ArrayList<ArCondicionadoModel> getTodosStatus(){
		return model.consultarTodos();
	}
}
