package Controller;

import javax.swing.JOptionPane;

import Logic.CatracaLogic;
import View.CatracaView;

public class CatracaController extends Controller
{
	private CatracaView view;
	private CatracaLogic logic;
	
	public CatracaController()
	{
		view = new CatracaView( this );
		logic = new CatracaLogic();
	}
	
	public void render()
	{
		view.show();
	}
	
	public void doLogin( String username, String password )
	{
		boolean success = false;
		
		try{
			success = logic.checkUser( username, password );

			if( success ){
				JOptionPane.showMessageDialog( null, lang.getString( "login.successTxt" ), lang.getString( "login.successTitle" ), 1 );
			}else{
				JOptionPane.showMessageDialog( null, lang.getString( "login.userPassError" ), lang.getString( "login.successTitle" ), 1 );
			}
		} catch( Exception e ) {
			JOptionPane.showMessageDialog( null, lang.getString( "login.readerError" ), lang.getString( "login.readerErrorTitle" ), 0 );
		}
	}
}