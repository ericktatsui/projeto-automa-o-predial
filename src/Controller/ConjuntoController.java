package Controller;

import java.util.ArrayList;

import Model.ConjuntoModel;
import View.SelecionarConjuntoView;

public class ConjuntoController extends Controller {
	private ConjuntoModel model;
	private SelecionarConjuntoView view;
	
	public ConjuntoModel renderDialog(){
		view = new SelecionarConjuntoView( this );
		view.showDialog();
		
		return (ConjuntoModel) view.getResult();
	}
	
	public ArrayList<ConjuntoModel> selectAll(){
		model = new ConjuntoModel();
		
		return model.selectAll();
	}
}
