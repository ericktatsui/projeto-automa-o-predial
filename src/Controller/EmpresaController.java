package Controller;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import Model.ConjuntoModel;
import Model.EmpresaModel;
import View.ConsultarEmpresaView;
import View.EmpresaView;
import View.SelecionarConjuntoView;

public class EmpresaController extends Controller {
	private ConsultarEmpresaView consultar;
	private EmpresaView view;
	private EmpresaModel model;
	private SelecionarConjuntoView selecionarConjunto;
	
	public EmpresaController(){
		model = new EmpresaModel();
	}
	
	public void renderConsultar(){
		consultar = new ConsultarEmpresaView( this );
		consultar.show();
	}

	public void renderEdit( int idEmpresa )
	{
		EmpresaModel empresa = model.consultarUm( idEmpresa );
		
		view = new EmpresaView( empresa, this, true );
		view.show();
	}
	
	public void renderAdd()
	{
		view = new EmpresaView( model, this );
		view.show();
	}
	
	public void edit( EmpresaModel empresa ){
		empresa.editar();
		
		JOptionPane.showMessageDialog(null, "Registro salvo com sucesso");
		
		consultar = new ConsultarEmpresaView( this );
		consultar.show();
	}
	
	public void adc( EmpresaModel empresa ){
		empresa.adicionar();
		
		JOptionPane.showMessageDialog(null, "Registro salvo com sucesso");
		
		consultar = new ConsultarEmpresaView( this );
		consultar.show();
	}
	
	public ArrayList<EmpresaModel> buscar( String termo ){
		return model.buscar( termo );
	}
	
	public boolean remover( int id ){
		return model.remover( id );
	}
	
	public ArrayList<EmpresaModel> consultarTodos(){
		return model.consultarTodos();
	}
}
