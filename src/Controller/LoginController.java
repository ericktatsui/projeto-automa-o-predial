package Controller;

import javax.swing.JOptionPane;

import Logic.LoginLogic;
import View.LoginView;

public class LoginController extends Controller
{
	private LoginView view;
	private LoginLogic logic;
	
	public LoginController()
	{
		view = new LoginView( this );
		logic = new LoginLogic();
	}
	
	public void render()
	{
		view.show();
	}
	
	public void doLogin( String username, String password )
	{
		boolean success = false;
		
		try{
			success = logic.checkUser( username, password );

			if( success ){
				JOptionPane.showMessageDialog( null, lang.getString( "login.successTxt" ), lang.getString( "login.successTitle" ), 1 );
				
				view.getMainFrame().initMenu();
				
				EmpresaController empresa = new EmpresaController();
				empresa.renderConsultar();
			}else{
				JOptionPane.showMessageDialog( null, lang.getString( "login.userPassError" ), lang.getString( "login.successTitle" ), 1 );
			}
		} catch( Exception e ) {
			JOptionPane.showMessageDialog( null, lang.getString( "login.readerError" ), lang.getString( "login.readerErrorTitle" ), 0 );
		}
	}
}