package Controller;

import View.TesteView;

public class TesteController extends Controller {
	private TesteView view;
	
	public TesteController()
	{
		view = new TesteView( this );
	}
	
	public void render()
	{
		view.show();
	}
}
