package Controller;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import Model.UserModel;
import View.UserView;
import View.ConsultarUsuarioView;


public class UserController extends Controller {
	private UserView view;
	private ConsultarUsuarioView consultar;
	private UserModel model;
	
	
	public UserController(){
		model = new UserModel();
	}
	
	public void renderConsultar(){
		consultar = new ConsultarUsuarioView(this);
		consultar.show();
	}
	
	public void renderEdit(int idUser){
		UserModel user = model.consultarUm(idUser);
		
		view = new UserView( user, this,true);
		view.show();
	}
	
	public void renderAdd(){
		view = new UserView(model, this);
		view.show();
	}
	
	public void edit(UserModel user){
		user.editar();
		
		JOptionPane.showMessageDialog(null, "Registro salvo com sucesso");
		
		consultar = new ConsultarUsuarioView(this);
		consultar.show();
	}
	
	public void adc(UserModel user){
		user.adicionar();
		
		JOptionPane.showMessageDialog(null, "Registro salvo com sucesso");
		
		consultar = new ConsultarUsuarioView(this);
		consultar.show();
	}
	
	
	public ArrayList<UserModel> buscar(String termo){
		return model.buscar(termo);
	}
	
	public boolean remover(int id){
		return model.remover(id);
	}

	public ArrayList<UserModel> consultarTodos(){
		return model.consultarTodos();
	}
}
