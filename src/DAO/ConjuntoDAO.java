package DAO;

import java.sql.ResultSet;

public class ConjuntoDAO {
	private DataAccess db;
	
	public ConjuntoDAO(){
		db = new DataAccess();
	}
	
	public ResultSet selectAll(){
		return db.query( "SELECT * FROM CONJUNTO" );
	}
}
