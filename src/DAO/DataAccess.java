package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class DataAccess {
	private String statusConn;
	private Connection conn;
	
	public DataAccess() {
		try{
			connect();
		} catch ( Exception e ) {
			JOptionPane.showInputDialog( "Erro ao conectar" );
		}
	}

	private Connection connect() throws InstantiationException, IllegalAccessException {
		conn = null;
		String driverName = "com.mysql.jdbc.Driver";
        String serverName = "localhost"; //caminho do servidor do BD
        String mydatabase = "automacao"; //nome do seu banco de dados
        String url = "jdbc:mysql://" + serverName + "/" + mydatabase;
        String username = "root"; //nome de um usu�rio de seu BD
        String password = "root"; //sua senha de acesso

        try {
        	Class.forName( driverName ).newInstance();
        	conn = DriverManager.getConnection(url, username, password);

        	if (conn != null) {
        		statusConn = "Conectado";
        	} else {
        		statusConn = "Desconectado";
        	}
        } catch ( ClassNotFoundException e ) {
        	System.out.println( "O driver expecificado nao foi encontrado." );
        } catch ( SQLException e ) {
        	System.out.println( "Nao foi possivel conectar ao Banco de Dados." );
        }

        return conn;
    }

    public String getStatus(){
    	return statusConn;
    }
    
    public void close() throws SQLException {
    	conn.close();
    }

    public ResultSet query( String sql ) {
    	PreparedStatement stm = null;
    	ResultSet rs = null;

    	try {
    		stm = conn.prepareStatement( sql );
    		rs = stm.executeQuery();
    	} catch ( Exception e ) {
    		e.printStackTrace();

    		try {
    			conn.rollback();
    		} catch ( SQLException e1 ) {
    			System.out.print( e1.getStackTrace() );
    		}
    	}

    	return rs;
    }
    
    public boolean nonQuery( String sql ) {
    	PreparedStatement stm = null;
    	boolean success = false;

    	try {
    		stm = conn.prepareStatement( sql );
    		success = stm.execute();
    	} catch ( Exception e ) {
    		e.printStackTrace();

    		try {
    			conn.rollback();
    		} catch ( SQLException e1 ) {
    			System.out.print( e1.getStackTrace() );
    		}
    	}

    	return success;
    }
}
