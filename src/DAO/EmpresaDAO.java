package DAO;

import java.sql.ResultSet;

import Model.EmpresaModel;

public class EmpresaDAO {
	DataAccess db;
	
	public EmpresaDAO()
	{
		db = new DataAccess();
	}
	
	public ResultSet consultarTodos()
	{
		return db.query( "SELECT * FROM EMPRESA" );
	}
	
	public ResultSet consultarUm( int id )
	{
		String sql = "SELECT *";
		sql += " FROM EMPRESA E";
		sql += " INNER JOIN CONJUNTO C ON (E.CONJUNTO = C.IDCONJUNTO)";
		sql += " WHERE IDEMPRESA = " + id;
		
		return db.query( sql );
	}
	
	public boolean remover( int id )
	{
		return db.nonQuery( "DELETE FROM EMPRESA WHERE IDEMPRESA = " + id );
	}
	
	public ResultSet buscar( String termo )
	{
		return db.query( "SELECT * FROM EMPRESA WHERE CNPJ like '%" + termo + "%' OR RAZAOSOCIAL like '%" + termo + "%'" );
	}
	
	public void editar( EmpresaModel empresa )
	{
		String sql = "UPDATE EMPRESA SET ";
		sql += " CNPJ = '" + empresa.getCNPJ() + "', ";
		sql += " RAZAOSOCIAL = '" + empresa.getRazaoSocial() + "', ";
		sql += " HRFUNCINI = '" + empresa.getHrFuncIni() + "', ";
		sql += " HRFUNCFIM = '" + empresa.getHrFuncFim() + "', ";
		sql += " TEMPMAX = '" + empresa.getTempMax() + "', ";
		sql += " HRARINI = '" + empresa.getHrArIni() + "', ";
		sql += " HRARFIM = '" + empresa.getHrArFim() + "', ";
		sql += " CONJUNTO = " + empresa.getConjunto().getIdConjunto();
		sql += " WHERE IDEMPRESA = " + empresa.getIdEmpresa();
		
		db.nonQuery( sql );
	}
	
	public void adicionar( EmpresaModel empresa )
	{
		String sql = "INSERT INTO EMPRESA (CNPJ, RAZAOSOCIAL, HRFUNCINI, HRFUNCFIM, TEMPMAX, HRARINI, HRARFIM, CONJUNTO) VALUES (";
		sql += " '" + empresa.getCNPJ() + "', ";
		sql += " '" + empresa.getRazaoSocial() + "', ";
		sql += " '" + empresa.getHrFuncIni() + "', ";
		sql += " '" + empresa.getHrFuncFim() + "', ";
		sql += empresa.getTempMax() + ", ";
		sql += " '" + empresa.getHrArIni() + "', ";
		sql += " '" + empresa.getHrArFim() + "', ";
		sql += empresa.getConjunto().getIdConjunto() + ") ";
		
		db.nonQuery( sql );
	}
}