package DAO;

import java.sql.ResultSet;

import javax.swing.JOptionPane;

import Model.UserModel;

public class UserDAO {

	DataAccess db;
	
	public UserDAO(){
		db = new DataAccess();		
	}
	
	public ResultSet consultarTodos(){
		return db.query("SELECT * FROM USUARIO");
	}
	
	public ResultSet consultarUm(int id){
		String sql = "SELECT * FROM USUARIO WHERE IDUSER = " + id;
		
		return db.query(sql);
	}
	
	public boolean remover(int id){
		return db.nonQuery("DELETE FROM USUARIO WHERE IDUSER = " + id);
	}
	
	public ResultSet buscar(String termo){

		return db.query("SELECT * FROM USUARIO WHERE NOME like '%"+termo+"%'");
	}
	
	public void editar(UserModel user){
		String sql = " UPDATE USUARIO SET ";
		sql += " NOME = '" + user.getNome() + "', ";
		sql += " CPF = '" + user.getCpf() + "' , ";
		sql += " RG = '" +  user.getRg() + "' , ";
		sql += " DATANASCIMENTO = '" + user.getDtNasc() + "' , ";
		sql += " EMAIL = '" + user.getEmail() +"' , ";
		sql += " TELEFONE = '" + user.getTel() +"' , ";
		sql += " CELULAR = '" + user.getCel() +"' , ";
		sql += " NIVEL = '" + user.getNvl() +"'";
		sql += " WHERE idUSER = " + user.getIdUsuario()+";";
		JOptionPane.showMessageDialog(null, sql);
		db.nonQuery(sql);
		
	}
	
	public void adicionar (UserModel user){
		
		String sql = " Insert into usuario (NOME,CPF,RG,DATANASCIMENTO,EMAIL,TELEFONE, CELULAR,NIVEL) VALUES (";
		sql += "'" + user.getNome() + "',";
		sql += "'" + user.getCpf() + "',";
		sql += "'" +  user.getRg() + "',";
		sql += "'" + user.getDtNasc() + "',";
		sql += "'" + user.getEmail() +"',";
		sql += "'" + user.getTel() +"',";
		sql += "'" + user.getCel() +"',";
		sql += "'" + user.getNvl() +"')";
		
		db.nonQuery(sql);
	}
}
