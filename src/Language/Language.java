package Language;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

public class Language {
	private static Language instance;

	private ResourceBundle bundle;
	private Locale locale;
	private String fileName;

	private ArrayList<Object> refreshersObj;
	private ArrayList<Method> refreshersMtd;

	//private Method refreshTexts;

	public Language() {
		fileName = "Language/Language";
		set( "pt", "BR" );

		refreshersObj = new ArrayList<Object>();
		refreshersMtd = new ArrayList<Method>();
	}

	public Locale set( String language, String country ) {
		locale = new Locale( language, country );
		Locale.setDefault( locale );

		bundle = ResourceBundle.getBundle( fileName, locale );

		return locale;
	}

	public void reset( String language, String country ) {
		set( language, country );
		
		int length = refreshersMtd.size();

		for( int i = 0; i < length; i++ ){
			try{
				refreshersMtd.get( i ).invoke( refreshersObj.get( i ), new Object[] {} );
			} catch ( IllegalArgumentException e ) {
				Throwable cause = e.getCause();
				System.out.println( cause.getMessage() );
			} catch ( Exception e ) {
				Throwable cause = e.getCause();
				System.out.println( cause.getMessage() );
			}
		}
	}

	public String getString( String name ) {
		return bundle.getString( name );
	}

	public static Language getInstance() {
		if( instance == null ) {
			instance = new Language();
		}

		return instance;
	}

	public Language setScreen( Object screen ) {
		try{
			Method mtd = screen.getClass().getMethod( "refreshTexts", new Class[] {} );

			refreshersMtd.add( mtd );
			refreshersObj.add( screen );
		} catch( Exception e ) {
			System.out.println( "M�todo refreshTexts() n�o existe na classe." );
		}

		return this;
	}

	public Locale getLocale() {
		return locale;
	}
}
