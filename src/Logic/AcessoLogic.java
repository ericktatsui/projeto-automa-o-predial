package Logic;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

import Model.AcessoModel;

public class AcessoLogic {

	   private String archive = "acessos.txt";

	   private Scanner readFile() throws Exception {
	      Scanner scanner;

	      try {
	         FileReader flReader = new FileReader( archive );
	         scanner = new Scanner( flReader );
	      } catch( Exception e ){
	         throw new Exception("Erro ao ler arquivo de texto.", e);
	      }

	      return scanner;
	   }

	   public ArrayList<AcessoModel> getAccess() {
		  ArrayList<AcessoModel> list = new ArrayList<>();
	      Scanner scanner;

	      try{
	         scanner = readFile();

	         while ( scanner.hasNextLine() ) {
	            String lines = scanner.nextLine();
	            String[] cols = lines.split( "=" );
	            
	            AcessoModel model = new AcessoModel();
	            model.setPerfil( cols[0] );
	            model.setNome( cols[1] );
	            model.setDataAcesso( cols[2] );
	            
	            list.add( model );	            
	         }
	      } catch( Exception e ){
	         System.out.print( "Erro ao ler arquivo de texto" );
	      }

	      return list;
	   }
}
