package Logic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class CatracaLogic {

	   private String archive = "usuariosCatraca.txt";

	   private Scanner readFile() throws Exception {
	      Scanner scanner;

	      try {
	         FileReader flReader = new FileReader( archive );
	         scanner = new Scanner( flReader );
	      } catch( Exception e ){
	         throw new Exception("Erro ao ler arquivo de texto.", e);
	      }

	      return scanner;
	   }

	   public boolean checkUser( String username, String pwd ) throws Exception {
	      boolean success = false;
	      Scanner scanner;
	      String password = EncryptLogic.MD5( pwd );

	      try{
	         scanner = readFile();

	         while ( scanner.hasNextLine() ) {
	            String userline = scanner.nextLine();
	            String[] userCols = userline.split( "=" );

	            //if( userCols[0].toLowerCase().equals( "manager" ) || userCols[0].toLowerCase().equals( "gerente" ) ){
	               if( username.toLowerCase().equals( userCols[1].toLowerCase() ) && password.toLowerCase().equals( userCols[2].toLowerCase() ) ){
	                  success = true;
	                  
	                  DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	                  String data = df.format(new Date()).toString();
	                  writeAcess(userCols[0], userCols[1], data);
	                  
	                  break;
	               }
	            //}
	         }
	      } catch( Exception e ){
	         throw new Exception("Erro ao ler arquivo de texto.", e);
	      }

	      return success;
	   }
	   
	   public void writeAcess( String perfil, String nome, String hrAcesso ){		   
		   try {				
				BufferedWriter buffWrite = new BufferedWriter( new FileWriter("acessos.txt", true) );
		        String linha = perfil + "=" + nome + "=" + hrAcesso;
		        buffWrite.append(linha + "\n");
		        buffWrite.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	   }
}
