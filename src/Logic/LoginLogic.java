package Logic;

import java.io.FileReader;
import java.util.Scanner;

public class LoginLogic {

	   private String archive = "usuariosLogin.txt";

	   private Scanner readFile() throws Exception {
	      Scanner scanner;

	      try {
	         FileReader flReader = new FileReader( archive );
	         scanner = new Scanner( flReader );
	      } catch( Exception e ){
	         throw new Exception("Erro ao ler arquivo de texto.", e);
	      }

	      return scanner;
	   }

	   public boolean checkUser( String username, String pwd ) throws Exception {
	      boolean success = false;
	      Scanner scanner;
	      String password = EncryptLogic.MD5( pwd );

	      try{
	         scanner = readFile();

	         while ( scanner.hasNextLine() ) {
	            String userline = scanner.nextLine();
	            String[] userCols = userline.split( "=" );

	            //if( userCols[0].toLowerCase().equals( "manager" ) || userCols[0].toLowerCase().equals( "gerente" ) ){
	               if( username.toLowerCase().equals( userCols[1].toLowerCase() ) && password.toLowerCase().equals( userCols[2].toLowerCase() ) ){
	                  success = true;
	                  break;
	               }
	            //}
	         }
	      } catch( Exception e ){
	         throw new Exception("Erro ao ler arquivo de texto.", e);
	      }

	      return success;
	   }
}
