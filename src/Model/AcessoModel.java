package Model;

public class AcessoModel {
	private String Perfil;
	private String Nome;
	private String DataAcesso;
	
	public String getPerfil() {
		return Perfil;
	}
	public void setPerfil(String perfil) {
		Perfil = perfil;
	}
	public String getNome() {
		return Nome;
	}
	public void setNome(String nome) {
		Nome = nome;
	}
	public String getDataAcesso() {
		return DataAcesso;
	}
	public void setDataAcesso(String dataAcesso) {
		DataAcesso = dataAcesso;
	}
}
