package Model;

import java.sql.ResultSet;
import java.util.ArrayList;

import DAO.ArCondicionadoDAO;

public class ArCondicionadoModel {
	private ArCondicionadoDAO dao;
	
	public ArCondicionadoModel()
	{
		dao = new ArCondicionadoDAO();
	}
	
	private EmpresaModel empresa;
	
	private String situacao;

	public EmpresaModel getEmpresa() {
		return empresa;
	}

	public void setEmpresa(EmpresaModel empresa) {
		this.empresa = empresa;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
	private ArrayList<ArCondicionadoModel> resultSetToModel( ResultSet rs ){
		ArrayList<ArCondicionadoModel> list = new ArrayList<ArCondicionadoModel>();
		double rand;

		try {
			while ( rs.next() ) {
				ArCondicionadoModel model = new ArCondicionadoModel();
				
				rand = Math.random();
				if( rand > 0.5 ){
					model.situacao = "Sim";
				}else{
					model.situacao = "N�o";
				}
				
				EmpresaModel empresa = new EmpresaModel();
				empresa.setIdEmpresa( rs.getInt( "IDEMPRESA" ) );
				empresa.setCNPJ( rs.getString( "CNPJ" ) );
				empresa.setRazaoSocial( rs.getString( "RAZAOSOCIAL" ) );
				empresa.setHrFuncIni( rs.getString( "HRFUNCINI" ) );
				empresa.setHrFuncFim( rs.getString( "HRFUNCFIM" ) );
				empresa.setTempMax( rs.getInt( "TEMPMAX" ) );
				empresa.setHrArIni( rs.getString( "HRARINI" ) );
				empresa.setHrArFim( rs.getString( "HRARFIM" ) );
				
				model.empresa = empresa;
				
				list.add( model );
			}
		} catch( Exception e ) {
			System.out.println( "N�o foi poss�vel converter ResultSet para ArrayList<EmpresaModel>" );
			e.printStackTrace();
		}

		return list;
	}
	
	public ArrayList<ArCondicionadoModel> consultarTodos() {
		return resultSetToModel( dao.consultarTodos() );
	}
}
