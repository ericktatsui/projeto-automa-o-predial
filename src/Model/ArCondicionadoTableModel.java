package Model;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
//import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
//import javax.swing.table.TableModel;

import Language.Language;

public class ArCondicionadoTableModel extends AbstractTableModel {
	private static final long serialVersionUID = -2190817059183799357L;
	
	protected Language lang = Language.getInstance().setScreen( this );
    private JTable jTable;
    protected ArrayList<ArCondicionadoModel> rows;
    protected String[] columns = new String[]{
    	lang.getString( "empresa.table.CNPJ" ),
    	lang.getString( "empresa.table.RazaoSocial" ),
    	lang.getString( "empresa.table.TempMax" ),
    	lang.getString( "empresa.table.HrArIni" ),
    	lang.getString( "empresa.table.HrArFim" ),
    	lang.getString( "ar.table.situacao" )
    };

    public ArCondicionadoTableModel( JTable table, ArrayList<ArCondicionadoModel> itemList ) {
        jTable = table;
        rows = new ArrayList<ArCondicionadoModel>( itemList );
    }

    public ArCondicionadoTableModel( JTable table ) {
        jTable = table;
        rows = new ArrayList<ArCondicionadoModel>();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public int getRowCount() {
        return rows.size();
    }

    @Override
    public String getColumnName( int columnIndex ) {
        return columns[columnIndex];
    }

    @Override
    public Object getValueAt( int rowIndex, int columnIndex ) {
        ArCondicionadoModel item = rows.get( rowIndex );
        EmpresaModel empresa = item.getEmpresa();
        Object value;

        // Pega os items pela coluna, deve ser colocado na ordem
        switch ( columnIndex ) {
        case 0:
        	value = empresa.getCNPJ();
        	break;
        case 1:
        	value = empresa.getRazaoSocial();
        	break;
        case 2:
        	value = empresa.getTempMax();
        	break;
        case 3:
        	value = empresa.getHrArIni();
        	break;
        case 4:
        	value = empresa.getHrArFim();
        	break;
        case 5:
        	value = item.getSituacao();
        	break;
        default:
        	value = null;
        	break;
        }

        return value;
    }

    @Override
    public boolean isCellEditable( int rowIndex, int columnIndex ) {
        return false;
    }

    public ArCondicionadoModel getItem( int rowIndex ) {
        ArCondicionadoModel item = null;

        if ( rowIndex < rows.size() ) {
            item = rows.get( rowIndex );
        }

        return item;
    }

    public int checkIfExists( ArCondicionadoModel itemArg ) {
        int index = -1;
        int length = rows.size();

        for( int i = 0; i < length; i++ ) {
            ArCondicionadoModel item = rows.get( i );

            if( item.equals( itemArg ) ) {
                index = i;
            }
        }

        return index;
    }

    public void addItem( ArCondicionadoModel itemArg ) {
         int index = checkIfExists( itemArg );

         ArCondicionadoModel item = rows.get( index );

         rows.set( index, item );

         fireTableDataChanged();
    }
    
    public void setRows( ArrayList<ArCondicionadoModel> items ) {
    	rows.clear();
    	rows.addAll( items );

        fireTableDataChanged();
   }

    public void clear() {
        rows.clear();
        fireTableDataChanged();
    }

    public boolean isEmpty() {
        return rows.isEmpty();
    }

    public Object getObject( int index ) {
        return rows.get( index );
    }

    public void setTable( JTable table ) {
        jTable = table;
    }

    public int getColumnByClick( MouseEvent e ){
        return jTable.getColumnModel().getColumnIndexAtX( e.getX() );
    }

    public int getRowByClick( MouseEvent e ){
        return e.getY() / jTable.getRowHeight();
    }

    public void refreshTexts() {
        JTableHeader th = jTable.getTableHeader();
        TableColumnModel tcm = th.getColumnModel();

    	tcm.getColumn( 0 ).setHeaderValue( lang.getString( "empresa.table.CNPJ" ) );
        tcm.getColumn( 1 ).setHeaderValue( lang.getString( "empresa.table.RazaoSocial" ) );
        tcm.getColumn( 2 ).setHeaderValue( lang.getString( "empresa.table.TempMax" ) );
        tcm.getColumn( 3 ).setHeaderValue( lang.getString( "empresa.table.HrIni" ) );
        tcm.getColumn( 4 ).setHeaderValue( lang.getString( "empresa.table.HrFim" ) );
        tcm.getColumn( 5 ).setHeaderValue( lang.getString( "ar.table.situacao" ) );
    }
}