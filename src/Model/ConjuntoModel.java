package Model;

import java.sql.ResultSet;
import java.util.ArrayList;

import DAO.ConjuntoDAO;

public class ConjuntoModel {
	private ConjuntoDAO dao;
	
	private int IdConjunto;
	private int Andar;
	private int Numero;
	private String Bloco;
	
	public ConjuntoModel(){
		dao = new ConjuntoDAO();
	}
	
	public int getIdConjunto() {
		return IdConjunto;
	}
	public void setIdConjunto(int idConjunto) {
		IdConjunto = idConjunto;
	}
	public int getAndar() {
		return Andar;
	}
	public void setAndar(int andar) {
		Andar = andar;
	}
	public int getNumero() {
		return Numero;
	}
	public void setNumero(int numero) {
		Numero = numero;
	}
	public String getBloco() {
		return Bloco;
	}
	public void setBloco(String bloco) {
		Bloco = bloco;
	}
	
	public ArrayList<ConjuntoModel> selectAll(){
		return resultSetToModel( dao.selectAll() );
	}
	
	private ArrayList<ConjuntoModel> resultSetToModel( ResultSet rs ){
		ArrayList<ConjuntoModel> list = new ArrayList<ConjuntoModel>();

		try {
			while ( rs.next() ) {
				ConjuntoModel model = new ConjuntoModel();
				model.IdConjunto = rs.getInt( "IDCONJUNTO" );
				model.Andar = rs.getInt( "ANDAR" );
				model.Numero = rs.getInt( "NUMERO" );
				model.Bloco = rs.getString( "BLOCO" );

				list.add( model );
			}
		} catch( Exception e ) {
			System.out.println( "N�o foi poss�vel converter ResultSet para ArrayList<EmpresaModel>" );
			e.printStackTrace();
		}

		return list;
	}
}
