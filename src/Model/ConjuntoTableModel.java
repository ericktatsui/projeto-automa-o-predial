package Model;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.awt.event.MouseEvent;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;

import Language.Language;

public class ConjuntoTableModel extends AbstractTableModel {
	private static final long serialVersionUID = -2190817059183799357L;
	
	protected Language lang = Language.getInstance().setScreen( this );
    private JTable jTable;
    protected ArrayList<ConjuntoModel> rows;
    protected String[] columns = new String[]{
    		lang.getString( "conjunto.table.id" ),
        	lang.getString( "conjunto.table.andar" ),
        	lang.getString( "conjunto.table.numero" ),
        	lang.getString( "conjunto.table.bloco" )
    };

    public ConjuntoTableModel( JTable table, ArrayList<ConjuntoModel> itemList ) {
        jTable = table;
        rows = new ArrayList<ConjuntoModel>( itemList );
    }

    public ConjuntoTableModel( JTable table ) {
        jTable = table;
        rows = new ArrayList<ConjuntoModel>();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public int getRowCount() {
        return rows.size();
    }

    @Override
    public String getColumnName( int columnIndex ) {
        return columns[columnIndex];
    }

    @Override
    public Object getValueAt( int rowIndex, int columnIndex ) {
        ConjuntoModel item = rows.get( rowIndex );
        Object value;

        // Pega os items pela coluna, deve ser colocado na ordem
        switch ( columnIndex ) {
		case 0:
        	value = item.getIdConjunto();
        	break;
        case 1:
        	value = item.getAndar();
        	break;
        case 2:
        	value = item.getNumero();
        	break;
        case 3:
        	value = item.getBloco();
        	break;
        default:
        	value = null;
        	break;
        }

        return value;
    }

    @Override
    public boolean isCellEditable( int rowIndex, int columnIndex ) {
        return false;
    }

    public ConjuntoModel getItem( int rowIndex ) {
        ConjuntoModel item = null;

        if ( rowIndex < rows.size() ) {
            item = rows.get( rowIndex );
        }

        return item;
    }

    public int checkIfExists( ConjuntoModel itemArg ) {
        int index = -1;
        int length = rows.size();

        for( int i = 0; i < length; i++ ) {
            ConjuntoModel item = rows.get( i );

            if( item.equals( itemArg ) ) {
                index = i;
            }
        }

        return index;
    }

    public void addItem( ConjuntoModel itemArg ) {
         int index = checkIfExists( itemArg );

         ConjuntoModel item = rows.get( index );

         rows.set( index, item );

         fireTableDataChanged();
    }
    
    public void setRows( ArrayList<ConjuntoModel> items ) {
    	rows.clear();
    	rows.addAll( items );

        fireTableDataChanged();
   }

    public void clear() {
        rows.clear();
        fireTableDataChanged();
    }

    public boolean isEmpty() {
        return rows.isEmpty();
    }

    public Object getObject( int index ) {
        return rows.get( index );
    }

    public void setTable( JTable table ) {
        jTable = table;
    }

    public int getColumnByClick( MouseEvent e ){
        return jTable.getColumnModel().getColumnIndexAtX( e.getX() );
    }

    public int getRowByClick( MouseEvent e ){
        return e.getY() / jTable.getRowHeight();
    }

    public void refreshTexts() {
        JTableHeader th = jTable.getTableHeader();
        TableColumnModel tcm = th.getColumnModel();

        tcm.getColumn( 0 ).setHeaderValue( lang.getString( "conjunto.table.id" ) );
        tcm.getColumn( 1 ).setHeaderValue( lang.getString( "conjunto.table.andar" ) );
        tcm.getColumn( 2 ).setHeaderValue( lang.getString( "conjunto.table.numero" ) );
        tcm.getColumn( 3 ).setHeaderValue( lang.getString( "conjunto.table.bloco" ) );
    }
}