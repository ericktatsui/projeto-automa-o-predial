package Model;

import java.sql.ResultSet;
import java.util.ArrayList;

import DAO.EmpresaDAO;

public class EmpresaModel {
	public EmpresaDAO dao;
	
	private int IdEmpresa;
	private String CNPJ;
	private String RazaoSocial;
	private String HrFuncIni;
	private String HrFuncFim;
	private int TempMax;
	private String HrArIni;
	private String HrArFim;
	private ConjuntoModel Conjunto;
	
	public EmpresaModel()
	{
		dao = new EmpresaDAO();
	}
	
	public ArrayList<EmpresaModel> consultarTodos()
	{
		ResultSet rs = dao.consultarTodos();
		return resultSetToModel( rs );
	}
	
	public ConjuntoModel getConjunto() {
		return Conjunto;
	}

	public void setConjunto(ConjuntoModel conjunto) {
		Conjunto = conjunto;
	}
	
	public void editar()
	{
		dao.editar( this );
	}
	
	public void adicionar()
	{
		dao.adicionar( this );
	}
	
	public boolean remover( int id )
	{
		return dao.remover( id );
	}
	
	public EmpresaModel consultarUm( int id )
	{
		ResultSet rs = dao.consultarUm( id );
		return resultSetToModelWithConj( rs ).get(0);
	}
	
	public ArrayList<EmpresaModel> buscar( String termo )
	{
		ResultSet rs = dao.buscar( termo );
		return resultSetToModel( rs );
	}
	
	private ArrayList<EmpresaModel> resultSetToModelWithConj( ResultSet rs ){
		ArrayList<EmpresaModel> list = new ArrayList<EmpresaModel>();

		try {
			while ( rs.next() ) {
				EmpresaModel model = new EmpresaModel();
				model.IdEmpresa = rs.getInt( "IDEMPRESA" );
				model.CNPJ = rs.getString( "CNPJ" );
				model.RazaoSocial = rs.getString( "RAZAOSOCIAL" );
				model.HrFuncIni = rs.getString( "HRFUNCINI" );
				model.HrFuncFim = rs.getString( "HRFUNCFIM" );
				model.TempMax = rs.getInt( "TEMPMAX" );
				model.HrArIni = rs.getString( "HRARINI" );
				model.HrArFim = rs.getString( "HRARFIM" );
				
				model.Conjunto = new ConjuntoModel();
				model.Conjunto.setIdConjunto( rs.getInt( "CONJUNTO" ) );
				model.Conjunto.setAndar( rs.getInt( "ANDAR" ) );
				model.Conjunto.setNumero( rs.getInt( "NUMERO" ) );
				model.Conjunto.setBloco( rs.getString( "BLOCO" ) );

				list.add( model );
			}
		} catch( Exception e ) {
			System.out.println( "N�o foi poss�vel converter ResultSet para ArrayList<EmpresaModel>" );
			e.printStackTrace();
		}

		return list;
	}
	
	private ArrayList<EmpresaModel> resultSetToModel( ResultSet rs ){
		ArrayList<EmpresaModel> list = new ArrayList<EmpresaModel>();

		try {
			while ( rs.next() ) {
				EmpresaModel model = new EmpresaModel();
				model.IdEmpresa = rs.getInt( "IDEMPRESA" );
				model.CNPJ = rs.getString( "CNPJ" );
				model.RazaoSocial = rs.getString( "RAZAOSOCIAL" );
				model.HrFuncIni = rs.getString( "HRFUNCINI" );
				model.HrFuncFim = rs.getString( "HRFUNCFIM" );
				model.TempMax = rs.getInt( "TEMPMAX" );
				model.HrArIni = rs.getString( "HRARINI" );
				model.HrArFim = rs.getString( "HRARFIM" );
				
				model.Conjunto = new ConjuntoModel();
				model.Conjunto.setIdConjunto( rs.getInt( "CONJUNTO" ) );

				list.add( model );
			}
		} catch( Exception e ) {
			System.out.println( "N�o foi poss�vel converter ResultSet para ArrayList<EmpresaModel>" );
			e.printStackTrace();
		}

		return list;
	}

	public int getIdEmpresa() {
		return IdEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		IdEmpresa = idEmpresa;
	}

	public String getCNPJ() {
		return CNPJ;
	}

	public void setCNPJ(String cNPJ) {
		CNPJ = cNPJ;
	}

	public String getRazaoSocial() {
		return RazaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		RazaoSocial = razaoSocial;
	}

	public String getHrFuncIni() {
		return HrFuncIni;
	}

	public void setHrFuncIni(String hrFuncIni) {
		HrFuncIni = hrFuncIni;
	}

	public String getHrFuncFim() {
		return HrFuncFim;
	}

	public void setHrFuncFim(String hrFuncFim) {
		HrFuncFim = hrFuncFim;
	}

	public int getTempMax() {
		return TempMax;
	}

	public void setTempMax(int tempMax) {
		TempMax = tempMax;
	}

	public String getHrArIni() {
		return HrArIni;
	}

	public void setHrArIni(String hrArIni) {
		HrArIni = hrArIni;
	}

	public String getHrArFim() {
		return HrArFim;
	}

	public void setHrArFim(String hrArFim) {
		HrArFim = hrArFim;
	}
}