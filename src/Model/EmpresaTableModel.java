package Model;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
//import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
//import javax.swing.table.TableModel;

import Language.Language;

public class EmpresaTableModel extends AbstractTableModel {
	private static final long serialVersionUID = -538180684479648047L;
	
	protected Language lang = Language.getInstance().setScreen( this );
    private JTable jTable;
    protected ArrayList<EmpresaModel> rows;
    protected String[] columns = new String[]{
    	lang.getString( "empresa.table.IdEmpresa" ),
    	lang.getString( "empresa.table.CNPJ" ),
    	lang.getString( "empresa.table.RazaoSocial" ),
    	lang.getString( "empresa.table.HrFuncIni" ),
    	lang.getString( "empresa.table.HrFuncFim" ),
    	lang.getString( "empresa.table.TempMax" ),
    	lang.getString( "empresa.table.HrArIni" ),
    	lang.getString( "empresa.table.HrArFim" ),
    };

    public EmpresaTableModel( JTable table, ArrayList<EmpresaModel> itemList ) {
        jTable = table;
        rows = new ArrayList<EmpresaModel>( itemList );
    }

    public EmpresaTableModel( JTable table ) {
        jTable = table;
        rows = new ArrayList<EmpresaModel>();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public int getRowCount() {
        return rows.size();
    }

    @Override
    public String getColumnName( int columnIndex ) {
        return columns[columnIndex];
    }

    @Override
    public Object getValueAt( int rowIndex, int columnIndex ) {
        EmpresaModel item = rows.get( rowIndex );
        Object value;

        // Pega os items pela coluna, deve ser colocado na ordem
        switch ( columnIndex ) {
        case 0:
        	value = item.getIdEmpresa();
        	break;
        case 1:
        	value = item.getCNPJ();
        	break;
        case 2:
        	value = item.getRazaoSocial();
        	break;
        case 3:
        	value = item.getHrFuncIni();
        	break;
        case 4:
        	value = item.getHrFuncFim();
        	break;
        case 5:
        	value = item.getTempMax();
        	break;
        case 6:
        	value = item.getHrArIni();
        	break;
        case 7:
        	value = item.getHrArFim();
        	break;
        default:
        	value = null;
        	break;
        }

        return value;
    }

    @Override
    public boolean isCellEditable( int rowIndex, int columnIndex ) {
        return false;
    }

    public EmpresaModel getItem( int rowIndex ) {
        EmpresaModel item = null;

        if ( rowIndex < rows.size() ) {
            item = rows.get( rowIndex );
        }

        return item;
    }

    public int checkIfExists( EmpresaModel itemArg ) {
        int index = -1;
        int length = rows.size();

        for( int i = 0; i < length; i++ ) {
            EmpresaModel item = rows.get( i );

            if( item.equals( itemArg ) ) {
                index = i;
            }
        }

        return index;
    }

    public void addItem( EmpresaModel itemArg ) {
         int index = checkIfExists( itemArg );

         EmpresaModel item = rows.get( index );

         rows.set( index, item );

         fireTableDataChanged();
    }
    
    public void setRows( ArrayList<EmpresaModel> items ) {
    	rows.clear();
    	rows.addAll( items );

        fireTableDataChanged();
   }

    public void clear() {
        rows.clear();
        fireTableDataChanged();
    }

    public boolean isEmpty() {
        return rows.isEmpty();
    }

    public Object getObject( int index ) {
        return rows.get( index );
    }

    public void setTable( JTable table ) {
        jTable = table;
    }

    public int getColumnByClick( MouseEvent e ){
        return jTable.getColumnModel().getColumnIndexAtX( e.getX() );
    }

    public int getRowByClick( MouseEvent e ){
        return e.getY() / jTable.getRowHeight();
    }

    public void refreshTexts() {
        JTableHeader th = jTable.getTableHeader();
        TableColumnModel tcm = th.getColumnModel();

        tcm.getColumn( 0 ).setHeaderValue( lang.getString( "empresa.table.IdEmpresa" ) );
        tcm.getColumn( 1 ).setHeaderValue( lang.getString( "empresa.table.CNPJ" ) );
        tcm.getColumn( 2 ).setHeaderValue( lang.getString( "empresa.table.RazaoSocial" ) );
        tcm.getColumn( 3 ).setHeaderValue( lang.getString( "empresa.table.HrFuncIni" ) );
        tcm.getColumn( 4 ).setHeaderValue( lang.getString( "empresa.table.HrFuncFim" ) );
        tcm.getColumn( 5 ).setHeaderValue( lang.getString( "empresa.table.TempMax" ) );
        tcm.getColumn( 6 ).setHeaderValue( lang.getString( "empresa.table.HrArIni" ) );
        tcm.getColumn( 7 ).setHeaderValue( lang.getString( "empresa.table.HrArFim" ) );
    }
}