package Model;

import java.sql.ResultSet;
import java.util.ArrayList;

import DAO.UserDAO;

public class UserModel {
	public UserDAO dao;
	
	private int IdUsuario;
	private String nome;
	private String cpf;
	private String rg;
	private String dtNasc;
	private String tel;
	private String cel;
	private String email;
	private String nvl;
	
	public UserModel(){
		dao = new UserDAO();
	}
	
	public ArrayList<UserModel> consultarTodos(){
		ResultSet rs = dao.consultarTodos();
		return resultSetToModel(rs);
	}
	
	public void editar(){
		dao.editar(this);
	}
	
	public void adicionar(){
		dao.adicionar(this);
	}
	
	public boolean remover(int id){
		return dao.remover(id);
	}
	
	
	public ArrayList<UserModel>buscar(String termo){
		ResultSet rs = dao.buscar(termo);
		return resultSetToModel(rs);
	}
	
	public UserModel consultarUm(int id){
		ResultSet rs = dao.consultarUm(id);
		return resultSetToModel(rs).get(0);
	}
	
	private ArrayList<UserModel> resultSetToModel(ResultSet rs){
		ArrayList<UserModel> list = new ArrayList<UserModel>();
		try{
			while(rs.next()){
				UserModel model = new UserModel();
				model.setIdUsuario(rs.getInt("IDUSER"));
				model.setNome(rs.getString("NOME"));
				model.setCpf(rs.getString("CPF"));
				model.setRg(rs.getString("RG"));
				model.setDtNasc(rs.getString("DATANASCIMENTO"));
				model.setEmail(rs.getString("EMAIL"));
				model.setTel(rs.getString("TELEFONE"));
				model.setCel(rs.getString("CELULAR"));
				model.setNvl(rs.getString("NIVEL"));
				
				list.add(model);
			}
		}catch(Exception e){
			System.out.println( "N�o foi poss�vel converter ResultSet para ArrayList<EmpresaModel>" );
			e.printStackTrace();
		}
		return list;
	}
	public UserDAO getDao() {
		return dao;
	}
	
	public void setDao(UserDAO dao) {
		this.dao = dao;
	}
	public int getIdUsuario() {
		return IdUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		IdUsuario = idUsuario;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getDtNasc() {
		return dtNasc;
	}
	public void setDtNasc(String dtNasc) {
		this.dtNasc = dtNasc;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getCel() {
		return cel;
	}
	public void setCel(String cel) {
		this.cel = cel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNvl() {
		return nvl;
	}
	public void setNvl(String nvl) {
		this.nvl = nvl;
	}	
}
