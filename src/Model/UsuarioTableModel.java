package Model;

import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;

import Language.Language;

public class UsuarioTableModel extends AbstractTableModel{

private static final long serialVersionUID = -538180684479648047L;
	
	protected Language lang = Language.getInstance().setScreen( this );
    private JTable jTable;
    protected ArrayList<UserModel> rows;
    
    protected String[] columns = new String[]{
    		lang.getString("usuario.id"),
    		lang.getString("usuario.nome"),
    		"CPF",
    		"RG",
    		lang.getString("usuario.dtNasc"),
    		"EMAIL", 
    		lang.getString("usuario.telefone"),
    		lang.getString("usuario.celular"), 
    		lang.getString("usuario.nivel")
    };

    public UsuarioTableModel( JTable table, ArrayList<UserModel> itemList ) {
        jTable = table;
        rows = new ArrayList<UserModel>( itemList );
    }

    public UsuarioTableModel( JTable table ) {
        jTable = table;
        rows = new ArrayList<UserModel>();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public int getRowCount() {
        return rows.size();
    }

    @Override
    public String getColumnName( int columnIndex ) {
        return columns[columnIndex];
    }

    @Override
    public Object getValueAt( int rowIndex, int columnIndex ) {
        UserModel item = rows.get( rowIndex );
        Object value;

        // Pega os items pela coluna, deve ser colocado na ordem
        switch ( columnIndex ) {
        case 0:
        	value = item.getIdUsuario();
        	break;
        case 1:
        	value = item.getNome();
        	break;
        case 2:
        	value = item.getCpf();
        	break;
        case 3:
        	value = item.getRg();
        	break;
        case 4:
        	value = item.getDtNasc();
        	break;
        case 5:
        	value = item.getEmail();
        	break;
        case 6:
        	value = item.getTel();
        	break;
        case 7:
        	value = item.getCel();
        	break;
        case 8:
        	value = item.getNvl();
        	break;
        default:
        	value = null;
        	break;
        }

        return value;
    }

    @Override
    public boolean isCellEditable( int rowIndex, int columnIndex ) {
        return false;
    }

    public UserModel getItem( int rowIndex ) {
        UserModel item = null;

        if ( rowIndex < rows.size() ) {
            item = rows.get( rowIndex );
        }

        return item;
    }

    public int checkIfExists( UserModel itemArg ) {
        int index = -1;
        int length = rows.size();

        for( int i = 0; i < length; i++ ) {
            UserModel item = rows.get( i );

            if( item.equals( itemArg ) ) {
                index = i;
            }
        }

        return index;
    }

    public void addItem( UserModel itemArg ) {
         int index = checkIfExists( itemArg );

         UserModel item = rows.get( index );

         rows.set( index, item );

         fireTableDataChanged();
    }
    
    public void setRows( ArrayList<UserModel> items ) {
    	rows.clear();
    	rows.addAll( items );

        fireTableDataChanged();
   }

    public void clear() {
        rows.clear();
        fireTableDataChanged();
    }

    public boolean isEmpty() {
        return rows.isEmpty();
    }

    public Object getObject( int index ) {
        return rows.get( index );
    }

    public void setTable( JTable table ) {
        jTable = table;
    }

    public int getColumnByClick( MouseEvent e ){
        return jTable.getColumnModel().getColumnIndexAtX( e.getX() );
    }

    public int getRowByClick( MouseEvent e ){
        return e.getY() / jTable.getRowHeight();
    }

    public void refreshTexts() {
        JTableHeader th = jTable.getTableHeader();
        TableColumnModel tcm = th.getColumnModel();

        tcm.getColumn( 0 ).setHeaderValue( lang.getString("usuario.id") );
        tcm.getColumn( 1 ).setHeaderValue( lang.getString("usuario.nome") );
        tcm.getColumn( 2 ).setHeaderValue( "CPF" );
        tcm.getColumn( 3 ).setHeaderValue( "RG");
        tcm.getColumn( 4 ).setHeaderValue( lang.getString("usuario.dtNasc"));
        tcm.getColumn( 5 ).setHeaderValue( "EMAIL");
        tcm.getColumn( 6 ).setHeaderValue( lang.getString("usuario.telefone") );
        tcm.getColumn( 7 ).setHeaderValue( lang.getString("usuario.celular"));
        tcm.getColumn( 8 ).setHeaderValue( lang.getString("usuario.nivel"));
    }
}
