package View;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import Controller.ArCondicionadoController;
import Model.ArCondicionadoModel;
import Model.ArCondicionadoTableModel;

public class ArCondicionadoView extends View implements iFrame {
	ArCondicionadoController controller;

	private JPanel container;

	private JTable tblAr;
	private ArCondicionadoTableModel tblModel;

	private JScrollPane scrollTbl;
	
	private JButton btnReconfig;

	public ArCondicionadoView( ArCondicionadoController ctrl ) {
		controller = ctrl;
	}

	public void initComponents() {
		container = new JPanel( new BorderLayout( 10, 10 ) );

		tblAr = new JTable();
		scrollTbl = new JScrollPane(tblAr);

		btnReconfig = new JButton("Reconfigurar");
	}

	public void addComponents() {
		container.add( scrollTbl, BorderLayout.CENTER );
		container.add( btnReconfig, BorderLayout.SOUTH );
	}

	public void configComponents() {
		container.setPreferredSize(new Dimension(width - 10, height - 10));
		
		//scrollTbl.setBorder( new EmptyBorder(10, 10, 10, 10) );
		scrollTbl.getInsets( new Insets(50, 50, 50, 50) );

		configTable();
		reconfigAct();
	}

	@Override
	public Component getScreen() {
		return container;
	}

	public void configTable() {
		//ArCondicionadoModel empresa = new ArCondicionadoModel();

		tblModel = new ArCondicionadoTableModel(tblAr, controller.getTodosStatus());

		tblAr.setModel(tblModel);
	}
	
	public void reconfigAct(){
		ActionListener act = new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				JOptionPane.showMessageDialog(null, "Arquivo de reconfiguração enviado com sucesso.");
			}
		};
		
		btnReconfig.addActionListener( act );
	}

	public void setComponentsValues() {
	}
}