package View;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import Controller.AcessoController;
import Logic.AcessoLogic;
import Model.AcessoModel;
import Model.AcessoTableModel;

public class ConsultarAcessoView extends View implements iFrame {
	AcessoController controller;

	private JPanel container;

	private JTable tbl;
	private AcessoTableModel tblModel;

	private JScrollPane scrollTbl;

	public ConsultarAcessoView( AcessoController ctrl ) {
		controller = ctrl;
	}

	public void initComponents() {
		container = new JPanel( new BorderLayout( 10, 10 ) );

		tbl = new JTable();
		scrollTbl = new JScrollPane(tbl);
	}

	public void addComponents() {
		container.add( scrollTbl, BorderLayout.CENTER );
	}

	public void configComponents() {
		container.setPreferredSize(new Dimension(width - 10, height - 10));
		
		scrollTbl.getInsets( new Insets(50, 50, 50, 50) );

		configTable();
	}

	@Override
	public Component getScreen() {
		return container;
	}

	public void configTable() {
		AcessoLogic logic = new AcessoLogic();

		tblModel = new AcessoTableModel(tbl, logic.getAccess());

		tbl.setModel(tblModel);
	}

	public void setComponentsValues() {
	}
}