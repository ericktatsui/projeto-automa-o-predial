package View;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import Controller.EmpresaController;
import Model.EmpresaModel;
import Model.EmpresaTableModel;

public class ConsultarEmpresaView extends View implements iFrame {
	EmpresaController controller;

	private JPanel container;
	private JPanel pnlTop;
	private JPanel pnlMiddle;
	private JPanel pnlBottom;

	private JTable tblEmpresas;
	private EmpresaTableModel tblModel;

	private JScrollPane scrollTbl;

	private JTextField txtSearch;

	private JButton btnSearch;
	private JButton btnRemove;
	private JButton btnEdit;

	public ConsultarEmpresaView( EmpresaController ctrl ) {
		controller = ctrl;
	}

	public void initComponents() {
		container = new JPanel(new FlowLayout(FlowLayout.LEFT));

		pnlTop = new JPanel(new FlowLayout(FlowLayout.LEFT));
		pnlMiddle = new JPanel(new FlowLayout(FlowLayout.LEFT));
		pnlBottom = new JPanel(new FlowLayout(FlowLayout.LEFT));

		tblEmpresas = new JTable();
		scrollTbl = new JScrollPane(tblEmpresas);

		txtSearch = new JTextField();
		
		btnSearch = new JButton("Buscar");
		btnRemove = new JButton("Remover selecionado");
		btnEdit = new JButton("Editar selecionado");
	}

	public void addComponents() {
		pnlTop.add(txtSearch);
		pnlTop.add(btnSearch);
		
		pnlMiddle.add(scrollTbl);
		
		pnlBottom.add(btnRemove);
		pnlBottom.add(btnEdit);

		container.add(pnlTop);
		container.add(pnlMiddle);
		container.add(pnlBottom);
	}

	public void configComponents() {
		container.setPreferredSize(new Dimension(width - 15, height));

		pnlTop.setPreferredSize(new Dimension(width, 30));
		pnlMiddle.setPreferredSize(new Dimension(width, height - 110));
		pnlBottom.setPreferredSize(new Dimension(width, 30));
		
		scrollTbl.setPreferredSize(new Dimension(width - 30, height - 120));
		System.out.println(width + "x" + height);
		
		txtSearch.setPreferredSize(new Dimension(200, 21));

		configTable();
		searchAct();
		deleteAct();
		editAct();
	}

	@Override
	public Component getScreen() {
		return container;
	}

	public void configTable() {
		EmpresaModel empresa = new EmpresaModel();

		tblModel = new EmpresaTableModel(tblEmpresas, empresa.consultarTodos());

		MouseAdapter doucleClick = new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				// l�gica para realizar a��o s� com duplo clique
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();

					int row = tblModel.getRowByClick(e);
					EmpresaModel empresa = (EmpresaModel) tblModel.getItem(row);

					controller.renderEdit( empresa.getIdEmpresa() );
				}
			}
		};

		tblEmpresas.addMouseListener(doucleClick);

		tblEmpresas.setModel(tblModel);
	}
	
	public void searchAct(){
		ActionListener act = new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				ArrayList<EmpresaModel> empresas = controller.buscar( txtSearch.getText() );
				
				tblModel.setRows( empresas );
			}
		};
		
		btnSearch.addActionListener( act );
		txtSearch.addActionListener( act );
	}
	
	public void deleteAct(){
		ActionListener act = new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				int row = tblEmpresas.getSelectedRow();
				EmpresaModel empresa = (EmpresaModel) tblModel.getItem(row);

				controller.remover( empresa.getIdEmpresa() );
				
				ArrayList<EmpresaModel> empresas = controller.consultarTodos();
				tblModel.setRows( empresas );
				
				JOptionPane.showMessageDialog(null, "Removido com sucesso!");
			}
		};
		
		btnRemove.addActionListener( act );
	}
	
	public void editAct(){
		ActionListener act = new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				int row = tblEmpresas.getSelectedRow();
				EmpresaModel empresa = (EmpresaModel) tblModel.getItem(row);
				
				controller.renderEdit( empresa.getIdEmpresa() );
			}
		};
		
		btnEdit.addActionListener( act );
	}

	public void setComponentsValues() {
	}
}