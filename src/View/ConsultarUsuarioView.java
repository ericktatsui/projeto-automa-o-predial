package View;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import Controller.UserController;
import Language.Language;
import Model.UserModel;
import Model.UsuarioTableModel;

public class ConsultarUsuarioView extends View implements iFrame{
	UserController controller;
	private Language lang;
	private JPanel container;
	private JPanel pnlTop;
	private JPanel pnlMiddle;
	private JPanel pnlBottom;

	private JTable tblUsuarios;
	private UsuarioTableModel tblModel;

	private JScrollPane scrollTbl;

	private JTextField txtSearch;

	private JButton btnSearch;
	private JButton btnRemove;
	private JButton btnEdit;

	public ConsultarUsuarioView( UserController ctrl ) {
		controller = ctrl;
	}

	public void initComponents() {
		lang =  Language.getInstance().setScreen( this );
		
		container = new JPanel(new FlowLayout(FlowLayout.LEFT));

		pnlTop = new JPanel(new FlowLayout(FlowLayout.LEFT));
		pnlMiddle = new JPanel(new FlowLayout(FlowLayout.LEFT));
		pnlBottom = new JPanel(new FlowLayout(FlowLayout.LEFT));

		tblUsuarios = new JTable();
		scrollTbl = new JScrollPane(tblUsuarios);

		txtSearch = new JTextField();
		
		btnSearch = new JButton(lang.getString("usuario.buscar"));
		btnRemove = new JButton(lang.getString("usuario.remove"));
		btnEdit = new JButton(lang.getString("usuario.editar"));
	}

	public void addComponents() {
		pnlTop.add(txtSearch);
		pnlTop.add(btnSearch);
		
		pnlMiddle.add(scrollTbl);
		
		pnlBottom.add(btnRemove);
		pnlBottom.add(btnEdit);

		container.add(pnlTop);
		container.add(pnlMiddle);
		container.add(pnlBottom);
	}

	public void configComponents() {
		container.setPreferredSize(new Dimension(width - 15, height));

		pnlTop.setPreferredSize(new Dimension(width, 30));
		pnlMiddle.setPreferredSize(new Dimension(width, height - 110));
		pnlBottom.setPreferredSize(new Dimension(width, 30));
		
		scrollTbl.setPreferredSize(new Dimension(width - 30, height - 120));
		System.out.println(width + "x" + height);
		
		txtSearch.setPreferredSize(new Dimension(200, 21));

		configTable();
		searchAct();
		deleteAct();
		editAct();
	}

	@Override
	public Component getScreen() {
		return container;
	}

	public void configTable() {
		UserModel usuario = new UserModel();

		tblModel = new UsuarioTableModel(tblUsuarios, usuario.consultarTodos());

		MouseAdapter doucleClick = new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				// l�gica para realizar a��o s� com duplo clique
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();

					int row = tblModel.getRowByClick(e);
					UserModel usuario = (UserModel) tblModel.getItem(row);

					controller.renderEdit( usuario.getIdUsuario() );
				}
			}
		};

		tblUsuarios.addMouseListener(doucleClick);

		tblUsuarios.setModel(tblModel);
	}
	
	public void searchAct(){
		ActionListener act = new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				ArrayList<UserModel> usuarios = controller.buscar( txtSearch.getText() );
				
				tblModel.setRows( usuarios );
			}
		};
		
		btnSearch.addActionListener( act );
		txtSearch.addActionListener( act );
	}
	
	public void deleteAct(){
		ActionListener act = new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				int row = tblUsuarios.getSelectedRow();
				UserModel usuario = (UserModel) tblModel.getItem(row);

				controller.remover( usuario.getIdUsuario() );
				
				ArrayList<UserModel> usuarios = controller.consultarTodos();
				tblModel.setRows( usuarios );
				
				JOptionPane.showMessageDialog(null, "Removido com sucesso!");
			}
		};
		
		btnRemove.addActionListener( act );
	}
	
	public void editAct(){
		ActionListener act = new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				int row = tblUsuarios.getSelectedRow();
				UserModel usuario = (UserModel) tblModel.getItem(row);
				
				controller.renderEdit( usuario.getIdUsuario() );
			}
		};
		
		btnEdit.addActionListener( act );
	}

	public void setComponentsValues() {
	}

	public void refreshTexts(){
		btnSearch.setText(lang.getString("usuario.buscar"));
		btnRemove.setText(lang.getString("usuario.remove"));
		btnEdit.setText(lang.getString("usuario.editar"));
	}
}
