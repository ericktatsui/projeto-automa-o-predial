package View;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import Controller.ConjuntoController;
import Controller.Controller;
import Controller.EmpresaController;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JSpinner;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.util.ArrayList;

import Language.Language;
import Model.ConjuntoModel;
import Model.EmpresaModel;

public class EmpresaView extends View implements iFrame {
	private EmpresaController controller;
	private EmpresaModel model;
	
	private Language lang;
	private JPanel pnlInfos;
	private JPanel pnlFunc;
	private JPanel container;
	private JPanel [] pnlFlows;
	private JLabel lblCnpj;
	private JLabel lblRazaoSocial;
	private JLabel lblConj;
	private JLabel lblHoraFunc;
	private JLabel lblTemp;
	private JLabel lblHoraAr;
	private JLabel lblAs;
	private JLabel lblAs1;
	private JScrollPane scrTela;
	private JTextField txtConj;
	private JTextField txtRazaoSocial;
	private JFormattedTextField ftxCnpj;
	private JComboBox cbbHoraFuncIni;
	private JComboBox cbbHoraFuncFim;
	private JComboBox cbbHoraArIni;
	private JComboBox cbbHoraArFim;
	private JButton btnSelec;
	private JButton btnSalvar;
	private JSpinner spnTempMax;
	
	public EmpresaView( EmpresaModel empresaModel, EmpresaController ctrl )
	{
		super();
		
		model = empresaModel;
		controller = ctrl;
		
	}
	
	public EmpresaView( EmpresaModel empresaModel, EmpresaController ctrl, boolean editMode )
	{
		super();
			
		model = empresaModel;
		toEdit = editMode;
		controller = ctrl;
	}
	
	public void initComponents() {
		lang =  Language.getInstance().setScreen( this );
		
		container = new JPanel(new BorderLayout());
		pnlInfos = new JPanel(new GridLayout(2, 1));
		pnlFunc = new JPanel(new GridLayout(4, 1));
		pnlFlows = new JPanel[6];
		
		scrTela = new JScrollPane();
		
		lblCnpj = new JLabel(lang.getString("empresa.lblCnpj"));
		lblRazaoSocial = new JLabel(lang.getString("empresa.lblRazaoSocial"));
		lblConj = new JLabel(lang.getString("empresa.lblConj"));
		lblHoraFunc = new JLabel(lang.getString("empresa.lblHoraFunc"));
		lblTemp = new JLabel(lang.getString("empresa.lblTemp"));
		lblHoraAr = new JLabel(lang.getString("empresa.lblHoraAr"));
		lblAs = new JLabel(lang.getString("empresa.lblAs"));
		lblAs1 =  new JLabel (lang.getString("empresa.lblAs"));
		
		txtConj = new JTextField();
		txtRazaoSocial = new JTextField();
		
		ftxCnpj = new JFormattedTextField(mascaraCnpj());
		
		btnSelec = new JButton(lang.getString("empresa.btnSelec"));
		btnSalvar = new JButton(lang.getString("empresa.betnSalvar"));
		
		cbbHoraFuncIni = new JComboBox<>(adcHoraComboBox());
		cbbHoraFuncFim = new JComboBox<>(adcHoraComboBox());
		cbbHoraArIni = new JComboBox<>(adcHoraComboBox());
		cbbHoraArFim = new JComboBox<>(adcHoraComboBox());
		
		spnTempMax = new JSpinner();
	}

	public void configComponents() {
		txtConj.setPreferredSize(new Dimension(200,25));
		txtRazaoSocial.setPreferredSize(new Dimension(200,25));
		ftxCnpj.setPreferredSize(new Dimension(150,25));
		spnTempMax.setPreferredSize(new Dimension(200,25));
		btnSalvar.setPreferredSize(new Dimension(100,25));
		
		saveAction();
		selecionarConjuntoAction();
	}
	
	private void selecionarConjuntoAction() {
		ActionListener act = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ConjuntoController conjuntoCtrl = new ConjuntoController();
				ConjuntoModel conjunto = conjuntoCtrl.renderDialog();
				
				txtConj.setText( conjunto.getIdConjunto() + "" );
			}
		};
		
		btnSelec.addActionListener( act );
	}

	public void fieldsValueToModel( EmpresaModel empresa ){
		empresa.setIdEmpresa( model.getIdEmpresa() );
		empresa.setCNPJ( ftxCnpj.getText() );
		empresa.setRazaoSocial( txtRazaoSocial.getText() );
		empresa.setHrFuncIni( cbbHoraFuncIni.getSelectedItem().toString() );
		empresa.setHrFuncFim( cbbHoraFuncFim.getSelectedItem().toString() );
		empresa.setHrArIni( cbbHoraArIni.getSelectedItem().toString() );
		empresa.setHrArFim( cbbHoraArFim.getSelectedItem().toString() );
		empresa.setTempMax( Integer.parseInt( spnTempMax.getValue().toString() ) );
		
		ConjuntoModel conjunto = new ConjuntoModel();
		conjunto.setIdConjunto( Integer.parseInt( txtConj.getText() ) );
		empresa.setConjunto( conjunto );
	}
	
	public void saveAction(){
		ActionListener act;
		
		EmpresaModel empresa = new EmpresaModel();
		
		if( toEdit ){
			act = new ActionListener() {
				public void actionPerformed(ActionEvent ev) {
					fieldsValueToModel( empresa );
					controller.edit( empresa );
				}
			};
		}else{
			act = new ActionListener() {
				public void actionPerformed(ActionEvent ev) {
					fieldsValueToModel( empresa );
					controller.adc( empresa );
				}
			};
		}
		
		btnSalvar.addActionListener( act );
	}
	
	@Override
	public void addComponents() {
		//Adiciona os componentes nas linhas
		for(int i = 0;i<pnlFlows.length;i++)
		{
			if(i == 0){
				pnlFlows[i] = new JPanel(new FlowLayout(FlowLayout.LEFT));
				pnlFlows[i].add(lblCnpj);
				pnlFlows[i].add(ftxCnpj);
				//adiciona as linhas nos grids
				pnlInfos.add(pnlFlows[i]);
			}else if(i==1){
				pnlFlows[i] = new JPanel(new FlowLayout(FlowLayout.LEFT));
				pnlFlows[i].add(lblRazaoSocial);
				pnlFlows[i].add(txtRazaoSocial);
				//adiciona as linhas nos grids
				pnlInfos.add(pnlFlows[i]);
			}else if(i==2){
				pnlFlows[i] = new JPanel(new FlowLayout(FlowLayout.LEFT));
				pnlFlows[i].add(lblConj);
				pnlFlows[i].add(txtConj);
				pnlFlows[i].add(btnSelec);
				//adiciona as linhas nos grids
				pnlFunc.add(pnlFlows[i]);
			}else if(i==3){
				pnlFlows[i] = new JPanel(new FlowLayout(FlowLayout.LEFT));
				pnlFlows[i].add(lblHoraFunc);
				pnlFlows[i].add(cbbHoraFuncIni);
				pnlFlows[i].add(lblAs);
				pnlFlows[i].add(cbbHoraFuncFim);
				//adiciona as linhas nos grids
				pnlFunc.add(pnlFlows[i]);
			}else if(i==4){
				pnlFlows[i] = new JPanel(new FlowLayout(FlowLayout.LEFT));
				pnlFlows[i].add(lblTemp);
				pnlFlows[i].add(spnTempMax);
				//adiciona as linhas nos grids
				pnlFunc.add(pnlFlows[i]);
			}else if(i==5){
				pnlFlows[i] = new JPanel(new FlowLayout(FlowLayout.LEFT));
				pnlFlows[i].add(lblHoraAr);
				pnlFlows[i].add(cbbHoraArIni);
				pnlFlows[i].add(lblAs1);
				pnlFlows[i].add(cbbHoraArFim);
				//adiciona as linhas nos grids
				pnlFunc.add(pnlFlows[i]);
			}
		}
		
		container.add(pnlInfos, BorderLayout.NORTH);
		container.add(pnlFunc, BorderLayout.CENTER);
		container.add(btnSalvar, BorderLayout.SOUTH);
	}
	
	public void setComponentsValues(){
		ftxCnpj.setText( model.getCNPJ() );
		txtRazaoSocial.setText( model.getRazaoSocial() );
		
		cbbHoraFuncIni.setSelectedItem( model.getHrFuncIni() );
		cbbHoraFuncFim.setSelectedItem( model.getHrFuncFim() );
		cbbHoraArIni.setSelectedItem( model.getHrArIni() );
		cbbHoraArFim.setSelectedItem( model.getHrArFim() );
		
		spnTempMax.setValue( model.getTempMax() );
		
		txtConj.setText( model.getConjunto().getIdConjunto() + "" );
	}

	public Component getScreen() {
		return container;
	}
	
	private String[] adcHoraComboBox()
	{
		String[] hrs = new String[288];
		String horaTx = "0";
		int hr = 0;
		int min = 0;
		
		for(int i = 0;i<hrs.length;i++)
		{
			if(min==55)
			{
				horaTx = Integer.toString(hr)+":"+Integer.toString(min);
				min=0;
				hr++;
			}else{
				horaTx = Integer.toString(hr)+":"+Integer.toString(min);
				min +=5;
			}
			hrs[i] = horaTx;
		}
		
		return hrs;
	}
	
	private MaskFormatter mascaraCnpj()
	{
		MaskFormatter mascaraCnpj = null;
		try
		{
			mascaraCnpj = new MaskFormatter("##.###.###/####-##");
			return mascaraCnpj;
		}
		catch(ParseException excp)
		{
			System.err.println("Erro na formatação: " + excp.getMessage());
			System.exit(-1);
		}
		return mascaraCnpj;
	}
	
	public void refreshTexts() {
		lblCnpj.setText(lang.getString("empresa.lblCnpj"));
		lblRazaoSocial.setText(lang.getString("empresa.lblRazaoSocial"));
		lblConj.setText(lang.getString("empresa.lblConj"));
		lblHoraFunc.setText(lang.getString("empresa.lblHoraFunc"));
		lblTemp.setText(lang.getString("empresa.lblTemp"));
		lblHoraAr.setText(lang.getString("empresa.lblHoraAr"));
		lblAs.setText(lang.getString("empresa.lblAs"));
		lblAs1.setText(lang.getString("empresa.lblAs"));
		btnSelec.setText(lang.getString("empresa.btnSelec"));
		btnSalvar.setText(lang.getString("empresa.btnSalvar"));
	}
}