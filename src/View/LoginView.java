package View;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import Controller.LoginController;
import Language.Language;
import Logic.LoginLogic;

public class LoginView extends View implements iFrame {	
	public Language lang;
	public LoginLogic logic;
	
	private LoginController controller;

	private JPanel container; // JPanel que vai conter todos componentes dessa tela
	private JPanel grid;

	private JLabel lblLogin;
	private JLabel lblPassword;

	private JTextField txtLogin;
	private JPasswordField txtPassword;

	private JButton btnLogin;

	public LoginView( LoginController ctrl ) {
		super();

		controller = ctrl;
		lang = Language.getInstance().setScreen( this );
		logic = new LoginLogic();
	}

	public void initComponents() {
		container = new JPanel( new GridBagLayout() );
		grid = new JPanel( new GridLayout( 0, 1, 5, 5 ) );

		lblLogin = new JLabel( lang.getString( "login.login" ) );
		lblPassword = new JLabel( lang.getString( "login.password" ) );

		txtLogin = new JTextField();
		txtPassword = new JPasswordField();

		btnLogin = new JButton( lang.getString( "login.button" ) );
	}

	public void addComponents() {
		container.add( grid );

		grid.add( lblLogin );
		grid.add( txtLogin );

		grid.add( lblPassword );
		grid.add( txtPassword );

		grid.add( btnLogin );
	}

	public void configComponents() {
		container.setPreferredSize( mainContainer.getSize() );

		txtLogin.setPreferredSize( new Dimension( 200, 21 ) );
		txtPassword.setPreferredSize( new Dimension( 200, 21 ) );

		loginClick();
		txtEnterAction();
	}

	public void refreshTexts() {
		lblLogin.setText( lang.getString( "login.login" ) );
		lblPassword.setText( lang.getString( "login.password" ) );

		btnLogin.setText( lang.getString( "login.button" ) );
	}

	/*
	 * @see Main.WindowFrame#getScreen()
	 * M�todo regrado pela interface iFrame
	 * � chamado por WindowFrame.show() para renderizar a tela. Deve retornar um container com todos os componentes da tela. 
	 */
	public Component getScreen() {
		return container;
	}

	private void tryLogin() {
		String username = txtLogin.getText();
		String password = String.valueOf( txtPassword.getPassword() ); // m�todo getText est� obsoleto, portante deve usar este que retorna um array de char, e estou convertendo para string.
		
		controller.doLogin( username, password );
	}

	private void loginClick() {
		ActionListener act = new ActionListener() {
			public void actionPerformed( ActionEvent event ) {
				tryLogin();
			}
		};

		btnLogin.addActionListener( act );
	}

	private void txtEnterAction() {
		ActionListener act = new ActionListener() {
			public void actionPerformed( ActionEvent event ) {
				tryLogin();
			}
		};

		txtPassword.addActionListener( act );
	}

	@Override
	public void setComponentsValues() {
		// TODO Auto-generated method stub
		
	}
}
