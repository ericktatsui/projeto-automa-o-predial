package View;

import java.awt.Dimension;

import javax.swing.JDialog;
import javax.swing.JFrame;

public class MainDialogView extends JDialog {
	private static final long serialVersionUID = 1L;
	
	private int width;
	private int height;
	private JFrame parent;

	public MainDialogView( JFrame parent, String title, int width, int height ) {
		super( parent, title, true );

		this.width = width;
		this.height = height;
		this.parent = parent;

		initFrame();
	}

	private void initFrame() {
		setDefaultCloseOperation( DISPOSE_ON_CLOSE );
		setResizable( false );
		setLocationRelativeTo( null );
		setSize( width, height );

		Dimension parentSize = parent.getSize();
		int parentWidth = parentSize.width;
		int parentHeight = parentSize.height;

		setLocation( (parentWidth / 2) - (width / 2), (parentHeight / 2) - (height / 2) );
	}

	public void showDialog() {
		setVisible( true );
	}

	public void closeDialog() {
		setVisible( false );
		dispose();
	}
}