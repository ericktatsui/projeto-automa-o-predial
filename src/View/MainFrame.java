package View;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import Controller.AcessoController;
import Controller.ArCondicionadoController;
import Controller.CatracaController;
import Controller.EmpresaController;
import Controller.UserController;
import Language.Language;

public class MainFrame extends JFrame {
	private static final long serialVersionUID = 1L;

	private static MainFrame instance;

	private JPanel pnl;

	private JMenu menuEmpresas;
	private JMenu menuUsuarios;
	private JMenu menuAcessos;
	private JMenu menuCatraca;
	private JMenu menuArCondicionado;
	private JMenu menuLanguage;

	private JMenuItem itemAddEmpresa;
	private JMenuItem itemEditEmpresa;
	private JMenuItem itemConsultarEmpresa;

	private JMenuItem itemPortuguese;
	private JMenuItem itemEnglish;
	private JMenuItem itemSpanish;
	
	private JMenuItem itemAddUser;
	private JMenuItem itemConsultarUser;

	private Language lang;
	
	private EmpresaController empresa;
	private UserController user;
	
	private int containerWidth;
	private int containerHeight;

	public MainFrame() {
		super();

		lang = Language.getInstance().setScreen( this );
		
		empresa = new EmpresaController();
		user = new UserController();
		
		setTitle( lang.getString( "title" ) );

		setSystemStyle();
		//initMenu();
		setContainer();
		initFrame();
	}

	private void initFrame() {
		setDefaultCloseOperation( EXIT_ON_CLOSE );
		setResizable( true );
		setLocationRelativeTo( null );
        setLocation( 0, 0 );

        setExtendedState( MAXIMIZED_BOTH );
        Rectangle rect = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
        
        containerWidth = (int)rect.getWidth();
        containerHeight = (int)rect.getHeight();
        
        setSize( containerWidth, containerHeight );

        setVisible( true );
	}

	private void setContainer() {
		pnl = new JPanel( new FlowLayout( FlowLayout.LEFT ) );
		
//		JScrollPane scroll = new JScrollPane(pnl);
//		scroll.setBorder(null);
		
		add( pnl );
	}

	public JPanel getFrameContainer() {
		return pnl;
	}

	private void setSystemStyle() {
		try {
			UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
		} 
		catch ( Exception e ) {
			System.out.println( "Erro ao carregar estilo do windows" );
		}
	}

	public void initMenu() {
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar( menuBar );

		menuEmpresas = new JMenu( lang.getString( "menu.empresas" ) );
		menuUsuarios = new JMenu( lang.getString( "menu.usuarios" ) );
		menuAcessos = new JMenu( lang.getString( "menu.acessos" ) );
		menuCatraca = new JMenu( lang.getString( "menu.catraca" ) );
		menuArCondicionado = new JMenu( lang.getString( "menu.arCondicionado" ) );
		
		menuLanguage = new JMenu( lang.getString( "menu.language" ) );

		menuBar.add( menuEmpresas );
		menuBar.add( menuUsuarios );
		menuBar.add( menuAcessos );
		menuBar.add( menuCatraca );
		menuBar.add( menuArCondicionado );
		menuBar.add( menuLanguage );

		menuItensEmpresa();
		menuItensUser();
		menuItemsLanguage();

		setLanguageActions();
		setEmpresaAction(); // A��es dos items de empresa
		setUserAction();
		setArAction();
		setAcessosAction();
		setCatracaAction();
	}

	private void menuItensEmpresa() {
		itemAddEmpresa = new JMenuItem( lang.getString( "menu.empresas.adicionar" ) );
		itemEditEmpresa = new JMenuItem( lang.getString( "menu.empresas.editar" ) );
		itemConsultarEmpresa = new JMenuItem( lang.getString( "menu.empresas.consultar" ) );

		menuEmpresas.add( itemAddEmpresa );
		menuEmpresas.add( itemEditEmpresa );
		menuEmpresas.add( itemConsultarEmpresa );
	}
	
	private void menuItensUser(){
		itemAddUser = new JMenuItem( lang.getString( "usuario.add" ) );
		itemConsultarUser = new JMenuItem( lang.getString( "usuario.consult" ) );
		
		menuUsuarios.add(itemAddUser);
		menuUsuarios.add(itemConsultarUser);
	}

	public void refreshTexts() {
		setTitle( lang.getString( "title" ) );

		menuEmpresas.setText( lang.getString( "menu.empresas" ) );
		menuUsuarios.setText( lang.getString( "menu.usuarios" ) );
		menuAcessos.setText( lang.getString( "menu.acessos" ) );
		menuCatraca.setText( lang.getString( "menu.catraca" ) );
		menuArCondicionado.setText( lang.getString( "menu.arCondicionado" ) );
		
		itemAddEmpresa.setText( lang.getString( "menu.empresas.adicionar" ) );
		itemEditEmpresa.setText( lang.getString( "menu.empresas.editar" ) );
		itemConsultarEmpresa.setText( lang.getString( "menu.empresas.consultar" ) );
		
		itemAddUser.setText( lang.getString( "usuario.add" ) );
		itemConsultarUser.setText( lang.getString( "usuario.consult" ) );
		
		menuLanguage.setText( lang.getString( "menu.language" ) );
	}

	private void menuItemsLanguage() {
		itemPortuguese = new JMenuItem( "Portugu�s" );
		itemEnglish = new JMenuItem( "English" );
		itemSpanish = new JMenuItem( "Espa�ol" );

		menuLanguage.add( itemPortuguese );
		menuLanguage.add( itemEnglish );
		menuLanguage.add( itemSpanish );
	}

	private void setEmpresaAction() {
		ActionListener add = new ActionListener() {
			public void actionPerformed( ActionEvent event ) {
				empresa.renderAdd();
			}
		};
		
		ActionListener edit = new ActionListener() {
			public void actionPerformed( ActionEvent event ) {
				empresa.renderAdd();
			}
		};
		
		ActionListener consultar = new ActionListener() {
			public void actionPerformed( ActionEvent event ) {
				empresa.renderConsultar();
			}
		};

		itemAddEmpresa.addActionListener( add );
		itemEditEmpresa.addActionListener( edit );
		itemConsultarEmpresa.addActionListener( consultar );
	}
	
	private void setUserAction(){
		ActionListener add = new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				user.renderAdd();
			}
		};
			
		ActionListener consultar = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				user.renderConsultar();
			}
		};

		itemAddUser.addActionListener(add);
		itemConsultarUser.addActionListener(consultar);		
	}
	
	private void setArAction(){
		MenuListener menuAct = new MenuListener() {
			public void menuSelected(MenuEvent ev) {
				ArCondicionadoController ar = new ArCondicionadoController();
				ar.render();
			}
			
			public void menuDeselected(MenuEvent ev) {				
			}
			
			public void menuCanceled(MenuEvent ev) {
			}
		};

		menuArCondicionado.addMenuListener(menuAct);		
	}
	
	private void setAcessosAction(){
		MenuListener menuAct = new MenuListener() {
			public void menuSelected(MenuEvent ev) {
				AcessoController acesso = new AcessoController();
				acesso.renderConsultar();
			}
			
			public void menuDeselected(MenuEvent ev) {
				//
			}
			
			public void menuCanceled(MenuEvent ev) {
				//
			}
		};

		menuAcessos.addMenuListener(menuAct);		
	}
	
	private void setCatracaAction(){
		MenuListener menuAct = new MenuListener() {
			public void menuSelected(MenuEvent ev) {
				CatracaController acesso = new CatracaController();
				acesso.render();
			}
			
			public void menuDeselected(MenuEvent ev) {
				//
			}
			
			public void menuCanceled(MenuEvent ev) {
				//
			}
		};

		menuCatraca.addMenuListener(menuAct);		
	}
	
	private void setLanguageActions() {
		changeLanguage( itemPortuguese, "pt", "BR" );
		changeLanguage( itemEnglish, "en", "US" );
		changeLanguage( itemSpanish, "es", "MX" );
	}

	private void changeLanguage( JMenuItem menuItem, String langStr, String countryStr ) {
		ActionListener act = new ActionListener() {
			public void actionPerformed( ActionEvent event ) {
				lang.reset( langStr, countryStr );
			}
		};

		menuItem.addActionListener( act );
	}

	public static MainFrame getInstance() {
		if( instance == null ) {
			instance = new MainFrame();
		}

		return instance;
	}
}
