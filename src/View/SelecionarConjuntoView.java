package View;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import Controller.ConjuntoController;
import Model.ConjuntoModel;
import Model.ConjuntoTableModel;

public class SelecionarConjuntoView extends View implements iDialog {
	private JPanel container;
	
	private ConjuntoController controller;

	private JTable tbl;
	private ConjuntoTableModel tblModel;

	private JScrollPane scrollTbl;
	
	private ConjuntoModel objReturn;
	
	public SelecionarConjuntoView( ConjuntoController ctrl ) {
		super( "Selecionar Conjunto", 600, 400 );
		
		controller = ctrl; 
	}

	public void initComponents() {
		container = new JPanel( new BorderLayout( 10, 10 ) );

		tbl = new JTable();
		scrollTbl = new JScrollPane(tbl);
	}
	
	public void addComponents() {
		container.add( scrollTbl, BorderLayout.CENTER );
	}

	public void configComponents() {		
		container.setBorder( new EmptyBorder( 10, 10, 10, 10) );

		configTable();
	}
	
	public void configTable() {
		ArrayList<ConjuntoModel> list = controller.selectAll();

		tblModel = new ConjuntoTableModel(tbl, list);
		tbl.setModel(tblModel);
		
		MouseAdapter doucleClick = new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				// l�gica para realizar a��o s� com duplo clique
				if (e.getClickCount() == 2 && !e.isConsumed()) {
					e.consume();

					int row = tblModel.getRowByClick(e);
					ConjuntoModel conjunto = (ConjuntoModel) tblModel.getItem(row);
					
					objReturn = conjunto;
					mainDialog.closeDialog();
				}
			}
		};

		tbl.addMouseListener(doucleClick);
	}

	public Component getScreen() {
		return container;
	}
	
	public void setComponentsValues() {
	}

	public Object getResult() {
		return objReturn;
	}
}
