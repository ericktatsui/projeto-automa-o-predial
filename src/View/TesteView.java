package View;

import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import Controller.TesteController;
import Language.Language;

public class TesteView extends View implements iFrame {
	private Language lang;
	private TesteController controller;

	private JPanel container;

	private JLabel lbl1;
	private JLabel lbl2;

	private JButton btn;

	public TesteView( TesteController ctrl )
	{
		super();

		lang = Language.getInstance().setScreen( this );
		controller = ctrl;
	}

	public void initComponents() {
		lang = Language.getInstance().setScreen( this );

		container = new JPanel( new GridLayout() );
		
		lbl1 = new JLabel( lang.getString( "teste.lbl1" ) );
		lbl2 = new JLabel( lang.getString( "teste.lbl2" ) );

		btn = new JButton( lang.getString( "teste.btn" ) );
	}

	public void addComponents() {
		container.add( lbl1 );
		container.add( lbl2);
		container.add( btn );
	}

	public void configComponents() {
		lbl1.setFont( new Font( lbl1.getFont().getName(), Font.PLAIN, 25 ) );
		lbl2.setFont( new Font( lbl2.getFont().getName(), Font.PLAIN, 25 ) );

		btnAction();
	}

	public void refreshTexts() {
		lbl1.setText( lang.getString( "teste.lbl1" ) );
		lbl2.setText( lang.getString( "teste.lbl2" ) );

		btn.setText( lang.getString( "teste.btn" ) );
	}

	public Component getScreen() {
		return container;
	}

	private void btnAction() {
		ActionListener act = new ActionListener() {
			public void actionPerformed( ActionEvent event ) {
				JOptionPane.showMessageDialog( null, "Teste" );
			}
		};

		btn.addActionListener( act );
	}

	public void resetScreen() {
		lbl1.setText( lang.getString( "teste.lbl1" ) );
		lbl2.setText( lang.getString( "teste.lbl2" ) );
	}

	@Override
	public void setComponentsValues() {
		// TODO Auto-generated method stub
		
	}
}
