package View;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import Controller.UserController;

public class UserAddView extends View implements iFrame {
	UserController controller;
	
	private JPanel container;
	
	private JScrollPane scrollTbl;
	
	public UserAddView( UserController ctrl ) {
		controller = ctrl;
	}

	@Override
	public void initComponents() {
		container = new JPanel();
		scrollTbl = new JScrollPane();
	}

	@Override
	public void configComponents() {
		scrollTbl.setPreferredSize( new Dimension(800, 500) );	
	}

	@Override
	public void addComponents() {
		container.add( scrollTbl );
	}

	@Override
	public Component getScreen() {
		return container;
	}

	@Override
	public void setComponentsValues() {
		// TODO Auto-generated method stub
		
	}
	
}
