package View;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import Controller.UserController;
import Language.Language;
import Model.ConjuntoModel;
import Model.EmpresaModel;
import Model.UserModel;

public class UserView extends View implements iFrame {
	UserController controller;
	UserModel model;
	
	private Language lang;
	private JPanel containerAux;
	private JPanel container;
	private JPanel pnlInfos;
	private JPanel pnlCont;
	private JPanel pnlOutr;
	private JPanel [] pnlFlows;
	private JLabel lblNome;
	private JLabel lblRg;
	private JLabel lblCpf;
	private JLabel lblDataNasc;
	private JLabel lblEmail;
	private JLabel lblTel;
	private JLabel lblCel;
	private JLabel lblNvl;
	private JTextField txtNome;
	private JTextField txtRg;
	private JTextField txtEmail;
	private JFormattedTextField ftxTel;
	private JFormattedTextField ftxCel;
	private JFormattedTextField ftxCpf;
	private JFormattedTextField ftxDataNasc;
	private JComboBox cbbNvl;
	private JButton btnSalvar;
	
	public UserView(UserModel userModel, UserController ctrl){
		super();
		
		model = userModel;
		controller = ctrl;
	}
	
	public UserView( UserModel userModel, UserController ctrl, boolean editMode)
	{
		super();
		
		model = userModel;
		controller = ctrl;
		toEdit = editMode;
	}
	
	public UserView( UserController ctrl ) {
		controller = ctrl;
	}

	@Override
	public void initComponents() {
		lang =  Language.getInstance().setScreen( this );
		
		containerAux = new JPanel(new BorderLayout());
		container = new JPanel(new BorderLayout());
		pnlInfos = new JPanel(new GridLayout(3,1));
		pnlCont = new JPanel(new GridLayout(2,1));
		pnlOutr = new JPanel(new GridLayout(1,1));
		pnlFlows = new JPanel[6];
		
		lblCel = new JLabel(lang.getString("usuario.celular"));
		lblCpf = new JLabel("CPF");
		lblRg = new JLabel("RG");
		lblDataNasc = new JLabel(lang.getString("usuario.dtNasc"));
		lblEmail = new JLabel("E-Mail");
		lblNome = new JLabel(lang.getString("usuario.nome"));
		lblNvl = new JLabel(lang.getString("usuario.nivel"));
		lblTel = new JLabel(lang.getString("usuario.telefone"));
		
		txtEmail = new JTextField();
		txtNome = new JTextField();
		txtRg = new JTextField();
		
		ftxTel = new JFormattedTextField(mascara("(##)####-####"));
		ftxCpf = new JFormattedTextField(mascara("###.###.###-##"));
		ftxDataNasc = new JFormattedTextField(mascara("##/##/####"));
		ftxCel = new JFormattedTextField(mascara("(##)#####-####"));
		
		cbbNvl = new JComboBox<>(adcNvlComboBox());
		
		btnSalvar = new JButton("Salvar");
		
	}

	@Override
	public void configComponents() {
		txtEmail.setPreferredSize(new Dimension(200,21));
		txtNome.setPreferredSize(new Dimension(200,21));
		txtRg.setPreferredSize(new Dimension(200,21));
		
		ftxTel.setPreferredSize(new Dimension(200,21));
		ftxCel.setPreferredSize(new Dimension(200,21));
		ftxCpf.setPreferredSize(new Dimension(200,21));
		ftxDataNasc.setPreferredSize(new Dimension(200,21));
		
		
		cbbNvl.setPreferredSize(new Dimension(200,21));
		
		btnSalvar.setPreferredSize(new Dimension(200,21));
		saveAction();
	}
	
	public void saveAction(){
		ActionListener act;
		
		UserModel user = new UserModel();
		
		if( toEdit ){
			act = new ActionListener() {
				public void actionPerformed(ActionEvent ev) {
					fieldsValueToModel( user );
					controller.edit( user );
				}
			};
		}else{
			act = new ActionListener() {
				public void actionPerformed(ActionEvent ev) {
					fieldsValueToModel( user );
					controller.adc( user );
				}
			};
		}
		
		btnSalvar.addActionListener( act );
	}
	
	public void fieldsValueToModel( UserModel user){
		user.setIdUsuario( model.getIdUsuario() );
		user.setNome(txtNome.getText());
		user.setCpf(ftxCpf.getText());
		user.setRg(txtRg.getText());
		user.setDtNasc(ftxDataNasc.getText());
		user.setEmail(txtEmail.getText());
		user.setTel(ftxTel.getText());
		user.setCel(ftxCel.getText());
		user.setNvl(cbbNvl.getSelectedItem().toString());
	}

	@Override
	public void addComponents() {
		for(int i = 0;i<pnlFlows.length;i++)
		{
			if(i == 0){
				pnlFlows[i] = new JPanel(new FlowLayout(FlowLayout.LEFT));
				pnlFlows[i].add(lblNome);
				pnlFlows[i].add(txtNome);
				//adiciona as linhas nos grids
				pnlInfos.add(pnlFlows[i]);
			}else if(i==1){
				pnlFlows[i] = new JPanel(new FlowLayout(FlowLayout.LEFT));
				pnlFlows[i].add(lblCpf);
				pnlFlows[i].add(ftxCpf);
				pnlFlows[i].add(lblRg);
				pnlFlows[i].add(txtRg);
				//adiciona as linhas nos grids
				pnlInfos.add(pnlFlows[i]);
			}else if(i==2){
				pnlFlows[i] = new JPanel(new FlowLayout(FlowLayout.LEFT));
				pnlFlows[i].add(lblDataNasc);
				pnlFlows[i].add(ftxDataNasc);
				
				//adiciona as linhas nos grids
				pnlCont.add(pnlFlows[i]);
			}else if(i==3){
				pnlFlows[i] = new JPanel(new FlowLayout(FlowLayout.LEFT));
				pnlFlows[i].add(lblEmail);
				pnlFlows[i].add(txtEmail);
				//adiciona as linhas nos grids
				pnlCont.add(pnlFlows[i]);
			}else if(i==4){
				pnlFlows[i] = new JPanel(new FlowLayout(FlowLayout.LEFT));
				pnlFlows[i].add(lblTel);
				pnlFlows[i].add(ftxTel);
				pnlFlows[i].add(lblCel);
				pnlFlows[i].add(ftxCel);
				
				//adiciona as linhas nos grids
				pnlCont.add(pnlFlows[i]);
			}else if(i==5){
				pnlFlows[i] = new JPanel(new FlowLayout(FlowLayout.LEFT));
				pnlFlows[i].add(lblNvl);
				pnlFlows[i].add(cbbNvl);
				//adiciona as linhas nos grids
				pnlOutr.add(pnlFlows[i]);
			}
		}
		
		containerAux.add(pnlInfos, BorderLayout.NORTH);
		containerAux.add(pnlCont, BorderLayout.CENTER);
		containerAux.add(pnlOutr, BorderLayout.SOUTH);
		
		container.add(containerAux, BorderLayout.CENTER);
		container.add(btnSalvar, BorderLayout.SOUTH);
	}

	@Override
	public Component getScreen() {
		return container;
	}

	@Override
	public void setComponentsValues() {
		txtNome.setText(model.getNome());
		txtEmail.setText(model.getEmail());
		txtRg.setText(model.getRg());
		
		ftxTel.setText(model.getTel());
		ftxCel.setText(model.getCel());
		ftxDataNasc.setText(model.getDtNasc());
		ftxCpf.setText(model.getCpf());
		
		cbbNvl.setSelectedItem(model.getNvl());
		
	}
	
	private String[] adcNvlComboBox(){
		String[] nvl = new String[3];
		nvl[0] = "Funcionario";
		nvl[1] = "Atendente";
		nvl[2] = "S�ndico";
		return nvl;
	}
	
	private MaskFormatter mascara(String formato)
	{
		MaskFormatter mascara = null;
		try
		{
			mascara = new MaskFormatter(formato);
			return mascara;
		}
		catch(ParseException excp)
		{
			System.err.println("Erro na formata��o: " + excp.getMessage());
			System.exit(-1);
		}
		return mascara;
	}	
	
	public void refreshTexts(){
		lblNome.setText(lang.getString("usuario.nome"));
		lblCel.setText(lang.getString("usuario.celular"));
		lblDataNasc.setText(lang.getString("usuario.dtNasc"));
		lblNvl.setText(lang.getString("usuario.nivel"));
		lblTel.setText(lang.getString("usuario.telefone"));
	}
}
