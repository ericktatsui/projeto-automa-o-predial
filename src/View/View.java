package View;

import javax.swing.JPanel;

public abstract class View implements iFrame, iDialog {
	private MainFrame mainFrame;
	protected MainDialogView mainDialog;
	public JPanel mainContainer;
	
	public final int width;
	public final int height;
	
	protected boolean toEdit;

	public View() {
		mainFrame = MainFrame.getInstance();
		mainContainer = mainFrame.getFrameContainer();

		width = mainContainer.getSize().width;
		height = mainContainer.getSize().height;
	}

	public View( String title, int w, int h ) {
		mainFrame = MainFrame.getInstance();
		mainDialog = new MainDialogView( mainFrame, title, w, h );
		
		width = w;
		height = h;
	}

	private void mountComponents() {
		initComponents();
		configComponents();
		addComponents();
	}

	private void resetScreen() {
		mainContainer.removeAll();
	}

	private void repaintScreen() {
		mainContainer.revalidate();
		mainFrame.repaint();
	}

	public void show() {
		resetScreen();
		mountComponents();
		
		if( toEdit ){
			setComponentsValues();
		}

		mainContainer.add( getScreen() );

		repaintScreen();
	}

	public void showDialog() {
		mountComponents();
		mainDialog.add( getScreen() );
		mainDialog.showDialog();
	}
	
	public MainFrame getMainFrame() {
		return mainFrame;
	}

}