package View;

import java.awt.Component;

import javax.swing.JPanel;

public interface iDialog {
	public JPanel container = new JPanel();
	public String title = "";
	public int dialogWidth = 100;
	public int dialogHeight = 100;

	public void initComponents();
	public void configComponents();
	public void addComponents();
	public Component getScreen();
}
