package View;

import java.awt.Component;

import javax.swing.JPanel;

public interface iFrame {
	public JPanel _container = new JPanel();

	public void initComponents();
	public void configComponents();
	public void addComponents();
	public Component getScreen();
	public void setComponentsValues();
}
